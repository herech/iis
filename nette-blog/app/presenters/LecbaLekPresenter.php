<?php

namespace App\Presenters;

use Nette;
use App\Model;
use Nette\Application\UI\Form;
use Tracy\Debugger;

/*
 * Prezenter který pracuje s propojením léčeb a léků - pracuje nad tabulkou spojení mezi těmito entitami v DB
 */
class LecbaLekPresenter extends BasePresenter
{
    private $database;
    
    // perzistentní parametry, které se předávají napříč presenterem
    
    /** @persistent */
    public $pridejLekKLecbePodleIDLecby; 
    /** @persistent */
    public $pridejLekKLecbePodleIDZvirete;
    /** @persistent */
    public $zobrazSouvisejiciLekyPodleIdLecby;
    /** @persistent */
    public $zobrazSouvisejiciLekyPodleIdZvirete;

    private $lekyProDanouLecbu;

    /*
     * Při startu se zkontroluje, zda je uživatel přihlášen
     */
    protected function startup()
    {
        parent::startup();

        if (!$this->user->isLoggedIn()) {
            if ($this->user->logoutReason === Nette\Security\IUserStorage::INACTIVITY) {
                $this->flashMessage('Byli jste odhlášeni kvůli své neaktivitě. Prosím, přihlašte se znovu.', 'alert-box info');
            }
            $this->redirect('Sign:in');
        }
    }

    /*
     * Prezenteru se předá databáze
     */
    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    /*
     * Připraví se zobrazení léků, které odpovídají určité léčbě 
     */
    public function renderDefault()
    {   
        $this->template->vypisLeky = false;
        $zobrazSouvisejiciLekyPodleIdLecby = $this->getParameter('podleIdLecby');
        if (!$zobrazSouvisejiciLekyPodleIdLecby) {
            $zobrazSouvisejiciLekyPodleIdLecby = $this->getParameter('zobrazSouvisejiciLekyPodleIdLecby');
        }

        $zobrazSouvisejiciLekyPodleIdZvirete = $this->getParameter('podleIdZvirete');
        if (!$zobrazSouvisejiciLekyPodleIdZvirete) {
            $zobrazSouvisejiciLekyPodleIdZvirete = $this->getParameter('zobrazSouvisejiciLekyPodleIdZvirete');
        }
        
        if ($zobrazSouvisejiciLekyPodleIdLecby && $zobrazSouvisejiciLekyPodleIdZvirete) {
            $lecba = $this->database->table('lecba')->where('poradove_cislo_lecby = ? AND ID_zvirete = ?',$zobrazSouvisejiciLekyPodleIdLecby, $zobrazSouvisejiciLekyPodleIdZvirete)->fetch();

            if (!$lecba) {
                $this->flashMessage('Daná léčba nebyla nalezena', 'alert-box alert');
                $this->redirect('Lecba:default');
            }
            $this->template->lecba = $lecba;
            $this->template->souvisejiciLekyPodleLecby = $this->database->table('lecba_lek')->where('poradove_cislo_lecby = ? AND ID_zvirete = ?',$zobrazSouvisejiciLekyPodleIdLecby, $zobrazSouvisejiciLekyPodleIdZvirete);
            $this->template->vypisLeky = true;
        }
    }
    
    /*
     * Připraví se zobrazení stránky, kde je možné předepsat lék v rámci dané léčby
     * @param  pridejLekKLecbePodleIDLecby    pořdové číslo léčby zvířete
     * @param  pridejLekKLecbePodleIDZvirete  id zvížete
     */
    public function renderCreate($pridejLekKLecbePodleIDLecby, $pridejLekKLecbePodleIDZvirete) {

        if(!$this->getUser()->isInRole('Doktor')) {
            $this->flashMessage('Nemáte oprávnění přidávat léky v rámci léčby', 'alert-box alert');
            $this->redirect('Lecba:default');
        }
    
        $lecba = $this->database->table('lecba')->where('poradove_cislo_lecby = ? AND ID_zvirete = ?',$pridejLekKLecbePodleIDLecby, $pridejLekKLecbePodleIDZvirete)->fetch();

        if (!$lecba) {
            $this->flashMessage('Daná léčba nebyla nalezena', 'alert-box alert');
            $this->redirect('Lecba:default');
        }
        $this->template->lecba = $lecba;

        $this->pridejLekKLecbePodleIDLecby = $pridejLekKLecbePodleIDLecby;
        $this->pridejLekKLecbePodleIDZvirete = $pridejLekKLecbePodleIDZvirete;
    }

    /*
     * Akce pro úpravu léku který byl předepsán pro danou léčbu
     * @param  $ID_leku  ID léku
     * @param  poradove_cislo_lecby    pořdové číslo léčby zvířete
     * @param  ID_zvirete  id zvížete
     */
    public function actionEdit($ID_leku, $poradove_cislo_lecby, $ID_zvirete)
    {
        if(!$this->getUser()->isInRole('Doktor')) {
            $this->flashMessage('Nemáte oprávnění upravovat léky v rámci léčby', 'alert-box alert');
            $this->redirect('LecbaLek:default');
        }
    
        if (!$ID_leku || !$poradove_cislo_lecby || !$ID_zvirete) {
            $this->flashMessage('Neplatné parametry', 'alert-box alert');
            $this->redirect('LecbaLek:default');
        }
    
        $lecba_lek = $this->database->table('lecba_lek')->where('ID_leku = ? AND poradove_cislo_lecby = ? AND ID_zvirete = ?',$ID_leku, $poradove_cislo_lecby, $ID_zvirete)->fetch();

        if ($lecba_lek) {
        
            $this->pridejLekKLecbePodleIDLecby = $poradove_cislo_lecby;
            $this->pridejLekKLecbePodleIDZvirete = $ID_zvirete;
        
            $nazev_leku = $this->database->table('lek')->get($ID_leku);
            
            $arr_lecba_lek = $lecba_lek->toArray();
            
            $arr_lecba_lek['datum_nasazeni'] = $arr_lecba_lek['datum_nasazeni']->format('d.m.Y');
            
            $this['pridejLekKLecbeForm']['ID_leku']->setItems(array($ID_leku => $nazev_leku->nazev));
            $this['pridejLekKLecbeForm']->setDefaults($arr_lecba_lek);
            $this['pridejLekKLecbeForm']['ID_leku']->setDisabled();
            
        }
        else {
            $this->flashMessage('Editace nemohla být provedena, zadané parametry jsou neplatné', 'alert-box alert');
            $this->redirect('default');
        }
    }
    
    /*
     * Připraví se zobrazení stránky, kde je možné editovat předepsaný lék v rámci dané léčby
     * @param  $ID_leku  ID léku
     * @param  poradove_cislo_lecby    pořdové číslo léčby zvířete
     * @param  ID_zvirete  id zvížete
     */
    public function renderEdit($ID_leku, $poradove_cislo_lecby, $ID_zvirete)
    {
        
        $lecba = $this->database->table('lecba')->where('poradove_cislo_lecby = ? AND ID_zvirete = ?', $poradove_cislo_lecby, $ID_zvirete)->fetch();

        if (!$lecba) {
            $this->flashMessage('Daná lék pro danou léčbu nebyl nalezen', 'alert-box alert');
            $this->redirect('LecbaLek:default');
        }
        $this->template->lecba = $lecba;
        
    }

    /*
     * Akce pro smazání léku který byl předepsán pro danou léčbu
     * @param  $ID_leku  ID léku
     * @param  poradove_cislo_lecby    pořdové číslo léčby zvířete
     * @param  ID_zvirete  id zvížete
     */
    public function actionDelete($ID_leku, $poradove_cislo_lecby, $ID_zvirete)
    {
        if(!$this->getUser()->isInRole('Doktor')) {
            $this->flashMessage('Nemáte oprávnění mazat léky v rámci léčby', 'alert-box alert');
            $this->redirect('LecbaLek:default');
        }
    
        if (!$ID_leku || !$poradove_cislo_lecby || !$ID_zvirete) {
            $this->flashMessage('Neplatné parametry', 'alert-box alert');
            $this->redirect('LecbaLek:default');
        }
    
        $lecba_lek = $this->database->table('lecba_lek')->where('ID_leku = ? AND poradove_cislo_lecby = ? AND ID_zvirete = ?',$ID_leku, $poradove_cislo_lecby, $ID_zvirete);

        if (!$lecba_lek) {
            $this->flashMessage('Lék pro danou léčbu nebyl nalezen', 'alert-box alert');
            $this->redirect('LecbaLek:default');
        }
        else {
            $lecba_lek->delete();
            $this->flashMessage('Lék byl u dané léčby smazán', 'alert-box success');
            $this->redirect('LecbaLek:default', array('podleIdLecby' => $poradove_cislo_lecby,'podleIdZvirete' => $ID_zvirete));
        }
    }
    
    /*
     * Akce pro podání léku který byl předepsán pro danou léčbu
     * @param  $ID_leku  ID léku
     * @param  poradove_cislo_lecby    pořdové číslo léčby zvířete
     * @param  ID_zvirete  id zvížete
     */
    public function actionPodatLek($ID_leku, $poradove_cislo_lecby, $ID_zvirete)
    {
        if (!$ID_leku || !$poradove_cislo_lecby || !$ID_zvirete) {
            $this->flashMessage('Neplatné parametry', 'alert-box alert');
            $this->redirect('LecbaLek:default');
        }
    
        $lecba_lek = $this->database->table('lecba_lek')->where('ID_leku = ? AND poradove_cislo_lecby = ? AND ID_zvirete = ?',$ID_leku, $poradove_cislo_lecby, $ID_zvirete);

        if (!$lecba_lek) {
            $this->flashMessage('Lék pro danou léčbu nebyl nalezen', 'alert-box alert');
            $this->redirect('LecbaLek:default');
        }
        else {
            // pokud zatím nikdo lék nepodal
            if (!isset($lecba_lek->ID_zamestnance)) {
                $lecba_lek->update(array('ID_zamestnance' => $this->getUser()->getIdentity()->getId()));
          
                $this->flashMessage('Lék byl u dané léčby podán zaměstnancem', 'alert-box success');
                $this->redirect('LecbaLek:default', array('podleIdLecby' => $poradove_cislo_lecby,'podleIdZvirete' => $ID_zvirete));
            }
            else {
                $this->flashMessage('Nelze opětovně podat lék', 'alert-box success');
                $this->redirect('LecbaLek:default');
            }
        }
    }

    /*
     * Formulář pro předepsání léku k léčbě
     */
    protected function createComponentPridejLekKLecbeForm()
    {
        $form = new Nette\Application\UI\Form;
        
        $form->addText('datum_nasazeni', 'Datum nasazení')
        ->setAttribute('placeholder', 'Datum nasazení')
        ->setDefaultValue(date('d.m.Y'))
        ->addFilter(function ($value) {
            return preg_replace("@^(\d{2})\\.(\d{2})\\.(\d{4})$@", "$3-$2-$1", $value);
        });
        
        $form->addText('doba_podavani', 'Doba podávání')
        ->setAttribute('placeholder', 'Doba podávání');
        
        $form->addText('davkovani', 'Dávkování')
        ->setAttribute('placeholder', 'Dávkování');
        
        $form->addHidden('ID_zvirete', $this->pridejLekKLecbePodleIDZvirete);
        $form->addHidden('poradove_cislo_lecby', $this->pridejLekKLecbePodleIDLecby);
        
            $tmpSelection = $this->database->table('lecba_lek')->where('poradove_cislo_lecby = ? AND ID_zvirete = ?',$this->pridejLekKLecbePodleIDLecby, $this->pridejLekKLecbePodleIDZvirete);
            $tmpSelection2 = $this->database->table('lek');
            $poleIDlekuVTabulce_lecba_lek = array();
            $poleIDlekuVTabulce_lek = array();
            foreach($tmpSelection as $tmpSelectionn) {
                $poleIDlekuVTabulce_lecba_lek[] = $tmpSelectionn->ID_leku;
            }
            foreach($tmpSelection2 as $tmpSelection2n) {
                $poleIDlekuVTabulce_lek[] = $tmpSelection2n->ID_leku;
            }
            
            $lekyProDanouLecbu = array_diff ( $poleIDlekuVTabulce_lek , $poleIDlekuVTabulce_lecba_lek);
        
        $neprirazeneLekyProDanouLecbu = $this->database->table('lek')->where('ID_leku', $lekyProDanouLecbu);
        $arrLeku = array();
        foreach ($neprirazeneLekyProDanouLecbu as $lek) {
            $arrLeku[$lek->ID_leku] = $lek->nazev;
        }
        $form->addSelect('ID_leku', 'Léky', $arrLeku);

        $form->addSubmit('send', 'Vložit do databáze')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'pridejLekKLecbeFormSucceeded');

        return $form;
    }

    /*
     * Akce která se zavolá po odeslání formuláře (předepsání léku pro léčbu)
     */
    public function pridejLekKLecbeFormSucceeded($form, array $values)
    {    
        if(!$this->getUser()->isInRole('Doktor')) {
            $this->flashMessage('Nemáte oprávnění přidávat léky v rámci léčby', 'alert-box alert');
            $this->redirect('Lecba:default');
        }
    
        $doslo_k_chybe = false;
    
        foreach ($values as & $value) if ($value === '' || $value === 0) $value=NULL;
        
            if (!preg_match('@^(\d{4})-(\d{2})-(\d{2})$@',$values['datum_nasazeni'], $matches)) {
                $form->addError('Datum nasazení není ve správném formátu.');
                $doslo_k_chybe = true;
            }
            else {
                $year = $matches[1];
                $day = $matches[3];
                $month = $matches[2];
                
                if (!checkdate ( $month , $day , $year )) {
                    $form->addError('Datum nasazení není ve správném formátu.');
                    $doslo_k_chybe = true;
                }
            }
        
        if ($doslo_k_chybe == false) {
        
            $ID_leku = $this->getParameter('ID_leku');
            $ID_zvirete = $this->getParameter('ID_zvirete');
            $poradove_cislo_lecby = $this->getParameter('poradove_cislo_lecby');
        
            //$values['ID_zamestnance'] = $this->getUser()->getIdentity()->getId();
        
            try {
            
                if ($ID_leku && $ID_zvirete && $poradove_cislo_lecby) {
                    $lecba_lek = $this->database->table('lecba_lek')->where('poradove_cislo_lecby = ? AND ID_leku = ? AND ID_zvirete = ?',$poradove_cislo_lecby, $ID_leku, $ID_zvirete )->fetch();
                    $lecba_lek->update($values);
                }
                else {
                    $this->database->table('lecba_lek')->insert($values);
                }

                $this->flashMessage('Data úspěšně uložena', 'alert-box success');
                $this->redirect('LecbaLek:default', array('podleIdLecby' => $values['poradove_cislo_lecby'],'podleIdZvirete' => $values['ID_zvirete']));

            }
            catch (Exception $e) {
                if ($e instanceof \Nette\Application\AbortException) {
                    throw $e;
                }
                $this->flashMessage('Chyba: Lék nebyl přidán k léčbě', 'alert-box alert');
                $this->redirect('Lecba:default');
            }
        }
    }

}
