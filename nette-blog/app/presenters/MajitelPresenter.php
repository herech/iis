<?php

namespace App\Presenters;

use Nette;
use App\Model;
use Nette\Application\UI\Form;
use Tracy\Debugger;

/*
 * Prezenter pro práci s majiteli zvířat
 */
class MajitelPresenter extends BasePresenter
{
    private $database;

    /*
     * Při startu se zkontroluje, zda je uživatel přihlášen
     */
    protected function startup()
    {
        parent::startup();

        if (!$this->user->isLoggedIn()) {
            if ($this->user->logoutReason === Nette\Security\IUserStorage::INACTIVITY) {
                $this->flashMessage('Byli jste odhlášeni kvůli své neaktivitě. Prosím, přihlašte se znovu.', 'alert-box info');
            }
            $this->redirect('Sign:in');
        }
    }

    /*
     * Prezenteru se předá databáze
     */
    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    /*
     * Připraví se majitelé pro zobrazení
     */
    public function renderDefault()
    {
        $this->template->vyhledej_majitele = false;

        // pokud chceme vyhledat majitele podne nějakého kritéria
        if ($this->getParameter('send') && ($this->getParameter('jmeno') || $this->getParameter('prijmeni') || $this->getParameter('mesto'))) {
            // vyhledávali jsme majitele
            $this->template->vyhledej_majitele = true;
            // vybereme všechny majitele
            $selection = $this->database->table('majitel');

            // připravíme řetězec vyhledávacích kritérií
            $searchParams = '';

            // procházíme předaná vyhledávací kritéria
            foreach($this->request->getParameters() as $key => $value) {
                // je to správné kritérium
                if ($key == 'jmeno' || $key == 'prijmeni' || $key == 'mesto') {
                    // je zadána požadovaná hodnota
                    if (!empty($value)) {
                        // přidáme kritérium do řetězce
                        $searchParams .= $key . '='. $value . ', ';

                        // z majitelů odebereme ty, kteří nevyhovují danému kritériu
                        $selection = $selection->where($key, $value);
                    }
                }
            }

            // předáme vyhledané majitele
            $this->template->majitele = $selection->order('prijmeni')->order('jmeno');
            // předáme řetězec kritérií bez ", " na konci
            $this->template->searchParams = substr($searchParams, 0, -2);
        }
        else { // nevyhledáváme majitele
            // předáme všechny majitele
            $this->template->majitele = $this->database->table('majitel')->order('prijmeni')->order('jmeno');
        }
    }

    /*
     * Formulář pro přidávání/úpravu majitele
     */
    protected function createComponentMajitelForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addText('jmeno', 'Jméno')
        ->setRequired('Prosím vyplňte své jméno')
        ->setAttribute('placeholder', 'Vaše jméno');

        $form->addText('prijmeni', 'Příjmení')
        ->setRequired('Prosím vyplňte své příjmení')
        ->setAttribute('placeholder', 'Vaše příjmení');

        $form->addText('ulice', 'Ulice')
        ->setAttribute('placeholder', 'Ulice');

        $form->addText('mesto', 'Město')
        ->setAttribute('placeholder', 'Město');

        // pokud je PSČ zadáno, musí být nezáporné celé číslo délky 5
        $form->addText('psc', 'Poštovní směrovací číslo')
        ->setAttribute('placeholder', 'PSČ')
        ->setAttribute('maxlength', 5)
        ->addCondition(Form::FILLED)
        ->addRule(Form::INTEGER, 'PSČ je číslo!')
        ->addRule(Form::RANGE, 'PSČ musí být nezáporné celé číslo', array(0, null))
        ->addRule(Form::LENGTH, 'PSČ má 5 čísel!', 5);

        // pokud je číslo účtu zadáno, musí být nezáporné celé číslo délky 10 - 14
        $form->addText('cislo_uctu', 'Číslo účtu')
        ->setAttribute('placeholder', 'Vaše číslo účtu')
        ->setAttribute('maxlength', 14)
        ->addCondition(Form::FILLED)
        ->addRule(Form::INTEGER, 'Číslo účtu je číslo!')
        ->addRule(Form::RANGE, 'Číslo účtu musí být nezáporné celé číslo', array(0, null))
        ->addRule(Form::MIN_LENGTH, 'Číslo účtu je příliš krátké!', 10)
        ->addRule(Form::MAX_LENGTH, 'Číslo účtu je příliš dlouhé!', 14);

        // pokud je RČ zadáno, musí být nezáporné celé číslo délky 10
        $form->addText('rodne_cislo', 'Rodné číslo')
        ->setAttribute('placeholder', 'Rodné číslo')
        ->setAttribute('maxlength', 10)
        ->addCondition(Form::FILLED)
        ->addRule(Form::INTEGER, 'Rodné číslo je číslo!')
        ->addRule(Form::RANGE, 'Rodné číslo musí být nezáporné celé číslo', array(0, null))
        ->addRule(Form::LENGTH, 'Rodné číslo má 10 čísel!', 10);

        // pokud je číslo OP zadáno, musí být nezáporné celé číslo délky 9
        $form->addText('cislo_OP', 'Číslo občanského průkazu')
        ->setAttribute('placeholder', 'Číslo OP')
        ->setAttribute('maxlength', 9)
        ->addCondition(Form::FILLED)
        ->addRule(Form::INTEGER, 'Číslo OP je číslo!')
        ->addRule(Form::RANGE, 'Číslo OP musí být nezáporné celé číslo', array(0, null))
        ->addRule(Form::LENGTH, 'Číslo OP má 9 čísel!', 9);

        // výčet pro výběr typu osoby
        $enum = array('fyzická osoba' => 'fyzická osoba','právnická osoba' => 'právnická osoba');
        // pokud je vybrána právnická osoba, jsou zobrazeny pole pro vyplnění IČO a DIČ
        // pokud je vybrána fyzická, tyto pole jsou skryta
        $form->addRadioList('typ', 'Typ', $enum)
        ->setDefaultValue('fyzická osoba')
        ->addCondition(Form::EQUAL, 'právnická osoba')
        ->toggle('cont_pravnicka_osoba');

        // pokud je IČO zadáno, musí být nezáporné celé číslo délky 8
        $form->addText('ICO', 'IČO')
        ->setAttribute('placeholder', 'IČO')
        ->setAttribute('maxlength', 8)
        ->addCondition(Form::FILLED)
        ->addRule(Form::INTEGER, 'IČO je číslo!')
        ->addRule(Form::RANGE, 'IČO musí být nezáporné celé číslo', array(0, null))
        ->addRule(Form::LENGTH, 'IČO má 8 čísel!', 8);

        // pokud je DIČ zadáno, musí být nezáporné celé číslo délky 12
        $form->addText('DIC', 'DIČ')
        ->setAttribute('placeholder', 'DIČ')
        ->setAttribute('maxlength', 12)
        ->addCondition(Form::FILLED)
        ->addRule(Form::INTEGER, 'DIČ je číslo!')
        ->addRule(Form::RANGE, 'DIČ musí být nezáporné celé číslo', array(0, null))
        ->addRule(Form::LENGTH, 'DIČ má 12 čísel!', 12);

        $form->addSubmit('send', 'Vložit do databáze')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'majitelFormSucceeded');

        return $form;
    }

    /*
     * Akce pro úpravu majitele
     * @param  $ID_majitele  ID upravovaného majitele
     */
    public function actionEdit($ID_majitele)
    {
        try {
            // z databáze si vytáhneme záznam upravovaného majitele
            $majitel = $this->database->table('majitel')->get($ID_majitele);

            // pokud tento záznam neexistuje, vypíšeme chybovou hlášku a vrátíme se na výchozí presenter pro majitele
            if (!$majitel) {
                $this->flashMessage('Majitel nebyl nalezen', 'alert-box alert');
                $this->redirect('default');
            }

            // pokud existuje, nastavíme jeho hodnoty do formuláře pro úpravu majitele jako výchozí
            $this['majitelForm']->setDefaults($majitel->toArray());
        }
        catch (\Exception $e) {
            if ($e instanceof \Nette\Application\AbortException) {
                throw $e;
            }
            $this->flashMessage('Chyba: Akce editace nemohla být provedena', 'alert-box alert');
            $this->redirect('default');
        }
    }

    /*
     * Akce pro smazání majitele
     * @param  $ID_majitele  ID mazaného majitele
     */
    public function actionDelete($ID_majitele)
    {
        try {
            // z databáze si vytáhneme záznam mazaného majitele
            $majitel = $this->database->table('majitel')->get($ID_majitele);

            if (!$majitel) { // pokud neexistuje, vypíšeme chybovou hlášku a vrátíme se na výchozí presenter
                $this->flashMessage('Majitel nebyl nalezen', 'alert-box alert');
                $this->redirect('default');
            }
            else { // pokud existuje, smažeme ho, vypíšeme hlášku o úspěchu a vrátíme se na výchozí presenter
                $majitel->delete();

                $this->flashMessage('Majitel byl smazán', 'alert-box success');
                $this->redirect('default');
            }
        }
        catch (\Exception $e) {
            if ($e instanceof \Nette\Application\AbortException) {
                throw $e;
            }
            $this->flashMessage('Chyba: Akce smazání nemohla být provedena', 'alert-box alert');
            $this->redirect('default');
        }
    }

    /*
     * Akce pro vkládání/úpravu majitele
     * @param  form    Formulář MajitelForm
     * @param  values  Hodnoty zadané ve formuláři
     */
    public function majitelFormSucceeded($form, $values)
    {
        $ID_majitele = $this->getParameter('ID_majitele');

        // pokud nebyla hodnota zadána, nastaví se na NULL
        foreach ($values as & $value) if ($value === '' || $value === 0) $value = NULL;

        if ($ID_majitele) { // pokud bylo zadáno ID_majitele, budeme upravovat
            // z databáze si vytáhneme záznam upravovaného majitele
            $majitel = $this->database->table('majitel')->get($ID_majitele);

            // pokud tento záznam neexistuje, vypíšeme chybovou hlášku a vrátíme se na výchozí presenter
            if (!$majitel) {
                $this->flashMessage('Majitel nebyl nalezen', 'alert-box alert');
                $this->redirect('default');
            }

            // aktualizujeme záznam
            $majitel->update($values);
        }
        else { // jinak budeme vkládat
            $majitel = $this->database->table('majitel')->insert($values);
        }

        // majitel úspěšně vložen/upraven, vypíšeme hlášku o úspěchu a vrátíme se na výchozí presenter
        $this->flashMessage('Data úspěšně uložena', 'alert-box success');
        $this->redirect('default');
    }

    /*
     * Formulář pro vyhledávání majitele
     */
    protected function createComponentSearchMajitelForm()
    {
        $form = new Nette\Application\UI\Form;

        // při odeslání formuláře se přesměrujeme na prezenster Majitel:default
        // a tam vypíšeme majitele podle zvolených kritérií
        $form->setAction($this->link('default'));
        $form->setMethod('GET');

        $form->addText('jmeno', 'Jméno')
        ->setDefaultValue('')
        ->setAttribute('placeholder', 'Jméno majitele');

        $form->addText('prijmeni', 'Příjmení')
        ->setDefaultValue('')
        ->setAttribute('placeholder', 'Příjmení majitele');

        $form->addText('mesto', 'Město')
        ->setDefaultValue('')
        ->setAttribute('placeholder', 'Bydliště majitele');

        $form->addSubmit('send', 'Vyhledej')->setAttribute('class', 'button postfix');

        return $form;
    }
}
