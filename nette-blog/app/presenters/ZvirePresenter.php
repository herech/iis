<?php

namespace App\Presenters;

use Nette;
use App\Model;
use Nette\Application\UI\Form;
use Tracy\Debugger;

/*
 * Prezenter pro práci se zvířaty
 */
class ZvirePresenter extends BasePresenter
{
    private $database;

    /*
     * Při startu se zkontroluje, zda je uživatel přihlášen
     */
    protected function startup()
    {
        parent::startup();

        if (!$this->user->isLoggedIn()) {
            if ($this->user->logoutReason === Nette\Security\IUserStorage::INACTIVITY) {
                $this->flashMessage('Byli jste odhlášeni kvůli své neaktivitě. Prosím, přihlašte se znovu.', 'alert-box info');
            }
            $this->redirect('Sign:in');
        }
    }

    /*
     * Prezenteru se předá databáze
     */
    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    /*
     * Připraví se zvířata pro zobrazení
     */
    public function renderDefault()
    {
        $zobraz_souvisejici_zvirata_podle_id_druhu = $this->getParameter('zobrazSouvisejiciZvirataPodleIdDruhu');
        $zobraz_souvisejici_zvirata_podle_id_majitele = $this->getParameter('zobrazSouvisejiciZvirataPodleIdMajitele');
        $this->template->vyhledej_zvirata = false;

        // bylo předáno ID druhu
        if ($zobraz_souvisejici_zvirata_podle_id_druhu) {
            // vybírají se zvířata podle druhu
            $this->template->zobraz_souvisejici_zvirata_podle_id_druhu = true;
            $this->template->zobraz_souvisejici_zvirata_podle_id_majitele = false;
            $this->template->druh = $this->database->table('druh')->get($zobraz_souvisejici_zvirata_podle_id_druhu);

            // pro zobrazení se předají pouze zvířata tohoto druhu
            $this->template->zvirata = $this->database->table('zvire')->where('ID_druhu', $zobraz_souvisejici_zvirata_podle_id_druhu)->order('jmeno');
        }
        // bylo předáno ID majitele
        else if ($zobraz_souvisejici_zvirata_podle_id_majitele) {
            // vybírají se zvířata podle majitele
            $this->template->zobraz_souvisejici_zvirata_podle_id_majitele = true;
            $this->template->zobraz_souvisejici_zvirata_podle_id_druhu = false;
            $this->template->majitel = $this->database->table('majitel')->get($zobraz_souvisejici_zvirata_podle_id_majitele);
            // pro zobrazení se předají pouze zvířata tohoto majitele
            $this->template->zvirata = $this->database->table('zvire')->where('ID_majitele', $zobraz_souvisejici_zvirata_podle_id_majitele)->order('jmeno');
        }
        // nezobrazovali jsme zvířata podle druhu ani podle majitele
        else {
            $this->template->zobraz_souvisejici_zvirata_podle_id_majitele = false;
            $this->template->zobraz_souvisejici_zvirata_podle_id_druhu = false;

            // pokud chceme vyhledat zvíře podle nějakého kriteria
            if ($this->getParameter('send') && ($this->getParameter('jmeno') || $this->getParameter('druh')) || $this->getParameter('pohlavi')) {
                    // vyhledávali jsme zvířata
                    $this->template->vyhledej_zvirata = true;
                    // vybere všechna zvířata
                    $selection = $this->database->table('zvire');

                    // připravíme řetězec vyhledávacích kritérií
                    $searchParams = "";

                    // procházíme předaná vyhledávací kritéria
                    foreach($this->request->getParameters() as $key => $value) {
                        // je to správné kritérium
                        if ($key == "druh" || $key == "jmeno" || $key == "pohlavi") {
                            // je zadána požadovaná hodnota
                            if (!empty($value)) {
                                // přidáme kritérium do řetězce
                                $searchParams .= $key . "=". $value . ", ";

                                // kvůli tomu, že název druhu je z jiné tabulky ($key = druh.nazev)
                                $key = ($key == 'druh') ? $key . ".nazev" : $key;
                                // ze zvířat odebereme ta, která nevyhovují danému kritériu
                                $selection = $selection->where($key, $value);
                            }
                        }
                    }

                    // předáme vyhledaná zvířata
                    $this->template->zvirata = $selection->order('jmeno');
                    // předáme řetězec kritérií bez ", " na konci
                    $this->template->searchParams = substr($searchParams, 0, -2);
            }
            else { // nevyhledáváme zvířata
                // předáme všechna zvířata
                $this->template->zvirata = $this->database->table('zvire')->order('jmeno');
            }
        }
    }

    /*
     * Formulář pro přidávání/úpravu zvířete
     */
    protected function createComponentZvireForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addText('jmeno', 'Jméno')
        ->setRequired('Prosím vyplňte jméno zvířete')
        ->setAttribute('placeholder', 'Jméno zvířete');

        // výběr majitele (z jiné tabulky)
        $majitele = $this->database->table('majitel')->order('prijmeni')->order('jmeno');
        $arrMajitelu = array();
        foreach ($majitele as $majitel) {
            $arrMajitelu[$majitel->ID_majitele] = $majitel->jmeno . " " . $majitel->prijmeni;
        }
        $form->addSelect('ID_majitele', 'Majitel', $arrMajitelu);

        // výběr druhu (z jiné tabulky)
        $druhy = $this->database->table('druh')->order('nazev');
        $arrDruhu = array();
        foreach ($druhy as $druh) {
            $arrDruhu[$druh->ID_druhu] = $druh->nazev;
        }
        $form->addSelect('ID_druhu', 'Druh', $arrDruhu);

        // výčet pro výběr pohlaví
        $enum = array('samec' => 'samec','samice' => 'samice');
        $form->addRadioList('pohlavi', 'Pohlaví', $enum)
        ->setDefaultValue('samec');

        $form->addText('barva', 'Barva')
        ->setAttribute('placeholder', 'Barva zvířete');

        // pokud je váha zadána, musí být nezáporné celé číslo maximální délky 10
        $form->addText('vaha', 'Váha')
        ->setAttribute('placeholder', 'Váha zvířete v kilogramech')
        ->setAttribute('maxlength', 10)
        ->addCondition(Form::FILLED)
        ->addRule(Form::INTEGER, 'Váha je číslo!')
        ->addRule(Form::RANGE, 'Váha musí být nezáporné celé číslo', array(0, null))
        ->addRule(Form::MAX_LENGTH, 'Zvíře je příliš těžké pro naši kliniku !!!', 10);

        // defaultně nastaveno aktuální datum
        // pokud je datum zadáno, musí být v požadovaném tvaru
        $form->addText('datum_narozeni', 'Datum narození')
        ->setDefaultValue(date('d.m.Y'))
        ->addCondition(Form::FILLED)
        ->addFilter(function ($value) {
            return preg_replace("@^(\d{2})\\.(\d{2})\\.(\d{4})$@", "$3-$2-$1", $value);
        });

        // defaultně nastaveno aktuální datum
        // pokud je datum zadáno, musí být v požadovaném tvaru
        $form->addText('dat_posl_prohl', 'Datum poslední prohlídky')
        ->setDefaultValue(date('d.m.Y'))
        ->addCondition(Form::FILLED)
        ->addFilter(function ($value) {
            return preg_replace("@^(\d{2})\\.(\d{2})\\.(\d{4})$@", "$3-$2-$1", $value);
        });

        $form->addSubmit('send', 'Vložit do databáze')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'zvireFormSucceeded');

        return $form;
    }

    /*
     * Akce pro úpravu zvířete
     * @param  $ID_zvirete  ID upravovaného zvirete
     */
    public function actionEdit($ID_zvirete)
    {
        // z databáze si vytáhneme záznam upravovaného zvířete
        $zvire = $this->database->table('zvire')->get($ID_zvirete);

        // pokud tento záznam neexistuje, vypíšeme chybovou hlášku a vrátíme se na výchozí presenter zvířete
        if (!$zvire) {
            $this->flashMessage('Zvíře nebylo nalezeno', 'alert-box alert');
            $this->redirect('default');
        }

        // prevedeme strukturu zaznamu na pole
        $arrZvire = $zvire->toArray();

        // pokud je datum v databázi
        if ($zvire['datum_narozeni'] != NULL) {
            // převede se na požadovaný formát pro výpis
            $arrZvire['datum_narozeni'] = $zvire['datum_narozeni']->format('d.m.Y');
        }
        else { // jinak se nastaví aktuální datum
            $arrZvire['datum_narozeni'] = date('d.m.Y');
        }

        // pokud je datum v databázi
        if ($zvire['dat_posl_prohl'] != NULL) {
            // převede se na požadovaný formát pro výpis
            $arrZvire['dat_posl_prohl'] = $zvire['dat_posl_prohl']->format('d.m.Y');
        }
        else { // jinak se nastaví aktuální datum
            $arrZvire['dat_posl_prohl'] = date('d.m.Y');
        }

        // nastavíme upravené hodnoty do formuláře pro úpravu zvířete jako výchozí
        $this['zvireForm']->setDefaults($arrZvire);
    }

    /*
     * Akce pro smazání zvířete
     * @param  $ID_zvirete  ID mazaného zvířete
     */
    public function actionDelete($ID_zvirete)
    {
        // z databáze si vytáhneme záznam mazaného zvířete
        $zvire = $this->database->table('zvire')->get($ID_zvirete);

        if (!$zvire) { // pokud neexistuje, vypíšeme chybovou hlášku a vrátíme se na výchozí presenter
            $this->flashMessage('Zvíře nebylo nalezeno', 'alert-box alert');
            $this->redirect('default');
        }
        else { // pokud existuje, smažeme ho, vypíšeme hlášku o úspěchu a vrátíme se na výchozí presenter
            $zvire->delete();

            $this->flashMessage('Zvíře bylo smazáno', 'alert-box success');
            $this->redirect('default');
        }
    }

    /*
     * Akce pro vkládání/úpravu zvířat
     * @param  form    Formulář ZvireForm
     * @param  values  Hodnoty zadané ve formuláři
     */
    public function zvireFormSucceeded($form, array $values)
    {
        $doslo_k_chybe = false;
        $ID_zvirete = $this->getParameter('ID_zvirete');

        // pokud nebyla hodnota zadána, nastaví se na NULL
        foreach ($values as & $value) if ($value === '' || $value === 0) $value = NULL;

        try {
            // datum bylo zadáno
            if ($values['datum_narozeni'] != NULL) {
                // kontroluje jestli bylo zadáno ve správném formátu
                if (!preg_match('@^(\d{4})-(\d{2})-(\d{2})$@',$values['datum_narozeni'], $matches)) {
                    $form->addError('Datum narození není ve správném formátu');
                    $doslo_k_chybe = true;
                }
                else {
                    $year = $matches[1];
                    $month = $matches[2];
                    $day = $matches[3];

                    // a jestli je to skutečně datum
                    if (!checkdate ( $month , $day , $year )) {
                        $form->addError('Datum narození není ve správném formátu');
                        $doslo_k_chybe = true;
                    }
                }
            }

            // datum bylo zadáno
            if ($values['dat_posl_prohl'] != NULL) {
                // kontroluje jestli bylo zadáno ve správném formátu
                if (!preg_match('@^(\d{4})-(\d{2})-(\d{2})$@',$values['dat_posl_prohl'], $matches)) {
                    $form->addError('Datum prohlídky není ve správném formátu');
                    $doslo_k_chybe = true;
                }
                else {
                    $year = $matches[1];
                    $month = $matches[2];
                    $day = $matches[3];

                    // a jestli je to skutečně datum
                    if (!checkdate ( $month , $day , $year )) {
                        $form->addError('Datum prohlídky není ve správném formátu');
                        $doslo_k_chybe = true;
                    }
                }
            }

            // pokud nedošlo k chybě
            if ($doslo_k_chybe == false) {
                if ($ID_zvirete) { // pokud bylo zadáno ID_zvirete, budeme upravovat
                    // z databáze si vytáhneme záznam upravovaného zvířete
                    $zvire = $this->database->table('zvire')->get($ID_zvirete);

                    if (!$zvire) { // pokud neexistuje, vypíšeme chybovou hlášku a vrátíme se na výchozí presenter
                        $this->flashMessage('Zvíře nebylo nalezeno', 'alert-box alert');
                        $this->redirect('default');
                    }

                    // aktualizujeme záznam
                    $zvire->update($values);
                }
                else { // jinak budeme vkládat
                    $zvire = $this->database->table('zvire')->insert($values);
                }

                // zvíře úspěšně vloženo/upraveno, vypíšeme hlášku o úspěchu a vrátíme se na výchozí presenter
                $this->flashMessage('Data úspěšně uložena', 'alert-box success');
                $this->redirect('default');
            }
        }
        catch (\Exception $e) {
            if ($e instanceof \Nette\Application\AbortException) {
                throw $e;
            }
            $this->flashMessage('Chyba: Data nemohla být uložena', 'alert-box alert');
            $this->redirect('default');
        }
    }

    /*
     * Formulář pro vyhledávání zvířete
     */
    protected function createComponentSearchZvireForm()
    {
        $form = new Nette\Application\UI\Form;

        // při odeslání formuláře se přesměrujeme na prezenster Zvire:default
        // a tam vypíšeme zvířata podle zvolených kritérií
        $form->setAction($this->link('default'));
        $form->setMethod('GET');

        $form->addText('jmeno', 'Jméno')
        ->setAttribute('placeholder', 'Jméno zvířete');

        $form->addText('druh', 'Druh')
        ->setAttribute('placeholder', 'Druh zvířete');

        $form->addText('pohlavi', 'Pohlaví')
        ->setAttribute('placeholder', 'Pohlaví zvířete');

        $form->addSubmit('send', 'Vyhledej')->setAttribute('class', 'button postfix');

        return $form;
    }
}
