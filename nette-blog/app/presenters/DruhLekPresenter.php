<?php

namespace App\Presenters;

use Nette;
use App\Model;
use Nette\Application\UI\Form;
use Tracy\Debugger;

/*
 * Prezenter který pracuje s propojením léků a druhů - pracuje nad tabulkou spojení mezi těmito entitami v DB
 */
class DruhLekPresenter extends BasePresenter
{
    private $database;
    
    // perzistentní parametry, které se předávají napříč presenterem
    
    /** @persistent */
    public $pridejLekKeDruhuPodleID;
    /** @persistent */
    public $pridejDruhKLekuPodleID;
    /** @persistent */
    public $zobrazSouvisejiciLekyPodleIdDruhu;
    /** @persistent */
    public $zobrazSouvisejiciDruhyPodleIdLeku;
    
    private $lekyProDanyDruh;
    private $druhyProDanyLek;

    /*
     * Při startu se zkontroluje, zda je uživatel přihlášen
     */
    protected function startup()
    {
        parent::startup();

        if (!$this->user->isLoggedIn()) {
            if ($this->user->logoutReason === Nette\Security\IUserStorage::INACTIVITY) {
                $this->flashMessage('You have been signed out due to inactivity. Please sign in again.', 'alert-box info');
            }
            $this->redirect('Sign:in');
        }
    }

    /*
     * Prezenteru se předá databáze
     */
    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    /*
     * Připraví se zobrazení léků, které odpovídají určitému druhu a vice versa
     */
    public function renderDefault()
    {   
        $this->template->zobrazSouvisejiciLekyPodleIdDruhu = false;
        $this->template->zobrazSouvisejiciDruhyPodleIdLeku = false;

        $zobrazSouvisejiciLekyPodleIdDruhu = $this->getParameter('podleIdDruhu');
        if (!$zobrazSouvisejiciLekyPodleIdDruhu) {
            $zobrazSouvisejiciLekyPodleIdDruhu = $this->getParameter('zobrazSouvisejiciLekyPodleIdDruhu');
        }
        $zobrazSouvisejiciDruhyPodleIdLeku = $this->getParameter('podleIdLeku');
        if (!$zobrazSouvisejiciDruhyPodleIdLeku) {
            $zobrazSouvisejiciDruhyPodleIdLeku = $this->getParameter('zobrazSouvisejiciDruhyPodleIdLeku');
        }
        
        // nejdřív zkusíme zjistit jestli uživatel nechce hledat související léky pro určitý druh zvířete a pokud ano, připravíme k zobrazení dané léky
        if ($zobrazSouvisejiciLekyPodleIdDruhu) {
            $druh = $this->database->table('druh')->get($zobrazSouvisejiciLekyPodleIdDruhu);

            // druh nebyl podle daného id nalezen
            if (!$druh) {
                $this->flashMessage('Daný druh nebyl nalezen', 'alert-box alert');
                $this->redirect('default');
            }
            $this->template->druh = $druh;
            $this->template->zobrazSouvisejiciLekyPodleIdDruhu = true;
            $this->template->souvisejiciLekyPodleDruhu = $this->database->table('druh_lek')->where('ID_druhu',$zobrazSouvisejiciLekyPodleIdDruhu);
        }
        // jinak zkusíme zjistit jestli uživatel nechce hledat související druhy zvířat pro určitý lék a pokud ano, připravíme k zobrazení dané druhy
        else if($zobrazSouvisejiciDruhyPodleIdLeku) {
            $lek = $this->database->table('lek')->get($zobrazSouvisejiciDruhyPodleIdLeku);

            // lék nebyl podle daného id nalezen
            if (!$lek) {
                $this->flashMessage('Daný lék nebyl nalezen', 'alert-box alert');
                $this->redirect('default');
            }
            $this->template->lek = $lek;
            $this->template->zobrazSouvisejiciDruhyPodleIdLeku = true;
            $this->template->souvisejiciDruhyPodleIdLeku = $this->database->table('druh_lek')->where('ID_leku',$zobrazSouvisejiciDruhyPodleIdLeku);
        }
    }
    
    /*
     * Funkce která kontroluje zobrazení stránky pro přidání léku k druhu a vice versa
     */
    public function renderCreate() {
        $this->template->pridejLekKeDruhuPodleID = false;
        $this->template->pridejDruhKLekuPodleID = false;

        $pridejLekKeDruhuPodleID = $this->getParameter('pridejLekKeDruhuPodleID');
        $pridejDruhKLekuPodleID = $this->getParameter('pridejDruhKLekuPodleID');
        
        // nejdřív zkusíme zjistit jestli uživatel nechce přidat léky pro určitý druh zvířete
        if ($pridejLekKeDruhuPodleID) {
            $druh = $this->database->table('druh')->get($pridejLekKeDruhuPodleID);
            
            // druh nebyl podle daného id nalezen
            if (!$druh) {
                $this->flashMessage('Daný druh nebyl nalezen', 'alert-box alert');
                $this->redirect('default');
            }
            $this->template->druh = $druh;
            $this->template->pridejLekKeDruhuPodleID = true;
            
            $this->pridejLekKeDruhuPodleID = $pridejLekKeDruhuPodleID;
            
        }
        // jinak zkusíme zjistit jestli uživatel nechce přidat druhy zvířat pro určitý lék
        else if($pridejDruhKLekuPodleID) {
            $lek = $this->database->table('lek')->get($pridejDruhKLekuPodleID);

            // lék nebyl podle daného id nalezen
            if (!$lek) {
                $this->flashMessage('Daný lék nebyl nalezen', 'alert-box alert');
                $this->redirect('default');
            }
            $this->template->lek = $lek;
            $this->template->pridejDruhKLekuPodleID = true;
            
            $this->pridejDruhKLekuPodleID = $pridejDruhKLekuPodleID;
        }
    } 
    
    /*
     * Akce pro úpravu léků pro daný druh a vice versa
     * @param  $upravLekyUDruhu - 'true' pokud se pro druh s id $ID_druhu budou upravovoat jeho léky
     *                           - 'false' pokud se pro lék s id $ID_leku budou upravovoat jeho druhy zvířat
     * @param  $ID_leku  ID léku
     * @param  $ID_nemoci  ID ID_druhu
     */
    public function actionEdit($upravLekyUDruhu, $ID_leku, $ID_druhu)
    {
        try {
            $druh_lek = $this->database->table('druh_lek')->where('ID_leku = ? AND ID_druhu = ?',$ID_leku, $ID_druhu)->fetch();

            // existuje nějaké propojení mezi daným lékem a daným druhem?
            if ($druh_lek) {
                $nazev_leku = $this->database->table('lek')->get($ID_leku);
                $nazev_druhu = $this->database->table('druh')->get($ID_druhu);
            
                $arr_druh_lek = $druh_lek->toArray();
                
                // pokud se pro druh s id $ID_druhu budou upravovoat jeho léky
                if ($upravLekyUDruhu == 'true') {
                    $this['pridejLekKeDruhuForm']['ID_leku']->setItems(array($ID_leku => $nazev_leku->nazev));
                    $this['pridejLekKeDruhuForm']->setDefaults($arr_druh_lek);
                    $this['pridejLekKeDruhuForm']['ID_leku']->setDisabled();
                }
                
                // pokud se pro lék s id $ID_leku budou upravovoat jeho druhy zvířat
                else if ($upravLekyUDruhu == 'false') {
                    $this['pridejDruhKLekuForm']['ID_druhu']->setItems(array($ID_druhu => $nazev_druhu->nazev));
                    $this['pridejDruhKLekuForm']->setDefaults($arr_druh_lek);
                    $this['pridejDruhKLekuForm']['ID_druhu']->setDisabled();
                }
            }
            // chyba, propojení neexistuje, není co upravovoat
            else {
                $this->flashMessage('Editace nemohla být provedena, zadané parametry jsou neplatné', 'alert-box alert');
                $this->redirect('default');
            }
        }
        catch (\Exception $e) {
            if ($e instanceof \Nette\Application\AbortException) {
                throw $e;
            }
            $this->flashMessage('Chyba: Akce editace nemohla být provedena', 'alert-box alert');
            $this->redirect('default');
        }
    }
    
    /*
     * Metoda pro přípravu vykreslení formuláře pro úpravu léků pro daný druh a vice versa
     * @param  $upravLekyUDruhu - 'true' pokud se pro druh s id $ID_druhu budou upravovoat jeho léky
     *                           - 'false' pokud se pro lék s id $ID_leku budou upravovoat jeho druhy zvířat
     * @param  $ID_leku  ID léku
     * @param  $ID_nemoci  ID ID_druhu
     */
    public function renderEdit($upravLekyUDruhu, $ID_leku, $ID_druhu)
    {
        $this->template->pridejLekKeDruhuPodleID = false;
        $this->template->pridejDruhKLekuPodleID = false;
        
        // pokud se pro druh s id $ID_druhu budou upravovoat jeho léky
        if ($upravLekyUDruhu == 'true') {
            $druh = $this->database->table('druh')->get($ID_druhu);

            $this->template->druh = $druh;
            $this->template->pridejLekKeDruhuPodleID = true;
            
            $this->pridejLekKeDruhuPodleID = $ID_druhu;
            
        }
        // pokud se pro lék s id $ID_leku budou upravovoat jeho druhy zvířat
        else if($upravLekyUDruhu == 'false') {
            $lek = $this->database->table('lek')->get($ID_leku);

            $this->template->lek = $lek;
            $this->template->pridejDruhKLekuPodleID = true;
            
            $this->pridejDruhKLekuPodleID = $ID_leku;
        }
    }
    
    /*
     * Akce pro smazání léku pro daný druh
     * @param  $ID_leku  ID léku
     * @param  $ID_druhu  ID druhu
     */
    public function actionDeleteLekUDruhu($ID_leku, $ID_druhu)
    {
        $lek = $this->database->table('druh_lek')->where('ID_leku = ? AND ID_druhu = ?',$ID_leku, $ID_druhu);

        // pokud lék s daným id neexistuje pro daný druh
        if (!$lek) {
            $this->flashMessage('Lék nebyl nalezen', 'alert-box alert');
            $this->redirect('default');
        }
        // smažeme lék pro daný druh zvířete
        else {
            $lek->delete();
            $this->flashMessage('Lék byl u daného druhu smazán', 'alert-box success');
            $this->redirect('default', array('podleIdDruhu' => $ID_druhu));
        }
    }
    
    /*
     * Akce pro smazání druhu pro daný lék
     * @param  $ID_leku  ID léku
     * @param  $ID_druhu  ID druhu
     */
    public function actionDeleteDruhULeku($ID_leku, $ID_druhu)
    {
        $druh = $this->database->table('druh_lek')->where('ID_leku = ? AND ID_druhu = ?',$ID_leku, $ID_druhu);

        // pokud druh s daným id neexistuje pro daný lék
        if (!$druh) {
            $this->flashMessage('Daný druh zvířete nebyl nalezen', 'alert-box alert');
            $this->redirect('default');
        }
        // smažeme druh pro daný lék
        else {
            $druh->delete();
            $this->flashMessage('Druh zvířete byl u daného léku smazán', 'alert-box success');
            $this->redirect('default', array('podleIdLeku' => $ID_leku));
        }
    }

    /*
     * Formulář pro přidávání/úpravu léku k/u druhu
     */
    protected function createComponentPridejLekKeDruhuForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addText('doporucene_davkovani', 'Doporučené dávkování')
        ->setAttribute('placeholder', 'Doporučené dávkování');
        
        $form->addText('vedlejsi_ucinky', 'Vedlejší účinky')
        ->setAttribute('placeholder', 'Vedlejší účinky');
        
        // pomocí skrytého pole rozliším druh ke které přidávám/upravuje lék
        $form->addHidden('ID_druhu', $this->getParameter('pridejLekKeDruhuPodleID'));

        // pro daný druh vytvoříme seznam léků, které mu ještě nebyly přiřazeny
            $tmpSelection = $this->database->table('druh_lek')->where('ID_druhu',$this->pridejLekKeDruhuPodleID);
            $tmpSelection2 = $this->database->table('lek');
            $poleIDlekuVTabulce_druh_lek = array();
            $poleIDlekuVTabulce_lek = array();
            foreach($tmpSelection as $tmpSelectionn) {
                $poleIDlekuVTabulce_druh_lek[] = $tmpSelectionn->ID_leku;
            }
            foreach($tmpSelection2 as $tmpSelection2n) {
                $poleIDlekuVTabulce_lek[] = $tmpSelection2n->ID_leku;
            }
            
            $lekyProDanyDruh = array_diff ( $poleIDlekuVTabulce_lek , $poleIDlekuVTabulce_druh_lek);
        
        $neprirazeneLekyProDanyDruhZvirete = $this->database->table('lek')->where('ID_leku', $lekyProDanyDruh);
        $arrLeku = array();
        foreach ($neprirazeneLekyProDanyDruhZvirete as $lek) {
            $arrLeku[$lek->ID_leku] = $lek->nazev;
        }
        $form->addSelect('ID_leku', 'Léky', $arrLeku);

        $form->addSubmit('send', 'Vložit do databáze')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'pridejLekKeDruhuFormSucceeded');

        return $form;
    }

    /*
     * Akce pro vkládání/úpravu léku u druhu
     * @param  form    Formulář LekKeDruhuForm
     * @param  values  Hodnoty zadané ve formuláři
     */
    public function pridejLekKeDruhuFormSucceeded($form, $values)
    {    
        // pokud nebyla hodnota zadána, nastaví se na NULL
        foreach ($values as & $value) if ($value === '' || $value === 0) $value=NULL;
        
        $ID_leku = $this->getParameter('ID_leku');
        $ID_druhu = $this->getParameter('ID_druhu');
        
        try {
            // pokud se jedná o úpravu přidání léku k druhu
            if ($ID_leku && $ID_druhu) {
                $druh_lek = $this->database->table('druh_lek')->where('ID_leku = ? AND ID_druhu = ?',$ID_leku, $ID_druhu)->fetch();
                $druh_lek->update($values);
            }
            // pokud se jedná o vložení nového léku k danému druhu
            else {
                $this->database->table('druh_lek')->insert($values);
            }
        
            $this->flashMessage('Data úspěšně uložena', 'alert-box success');
            $this->redirect('default', array('podleIdDruhu' => $values->ID_druhu));
            
        }
        catch (\Exception $e) {
            if ($e instanceof \Nette\Application\AbortException) {
                throw $e;
            }
            $this->flashMessage('Chyba: Lék nebyl přidán k druhu zvířete', 'alert-box alert');
            $this->redirect('Druh:default');
        }
    }
    
    /*
     * Formulář pro přidávání/úpravu druhů k/u léku
     */
    protected function createComponentPridejDruhKLekuForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addText('doporucene_davkovani', 'Doporučené dávkování')
        ->setAttribute('placeholder', 'Doporučené dávkování');
        
        $form->addText('vedlejsi_ucinky', 'Vedlejší účinky')
        ->setAttribute('placeholder', 'Vedlejší účinky');
        
        // pomocí skrytého pole rozliším lék ke kterému přidávám/upravuji lék
        $form->addHidden('ID_leku', $this->getParameter('pridejDruhKLekuPodleID'));


        // pro daný lék vytvoříme seznam druhů, které mu ještě nebyly přiřazeny
            $tmpSelection = $this->database->table('druh_lek')->where('ID_leku',$this->pridejDruhKLekuPodleID);
            $tmpSelection2 = $this->database->table('druh');
            $poleIDdruhuVTabulce_druh_lek = array();
            $poleIDdruhuVTabulce_druh = array();
            foreach($tmpSelection as $tmpSelectionn) {
                $poleIDdruhuVTabulce_druh_lek[] = $tmpSelectionn->ID_druhu;
            }
            foreach($tmpSelection2 as $tmpSelection2n) {
                $poleIDdruhuVTabulce_druh[] = $tmpSelection2n->ID_druhu;
            }
            
            $druhyProDanyLek = array_diff ( $poleIDdruhuVTabulce_druh , $poleIDdruhuVTabulce_druh_lek);
        
        $neprirazeneDruhyZvireteProDanyLek = $this->database->table('druh')->where('ID_druhu', $druhyProDanyLek);
        $arrDruhu = array();
        foreach ($neprirazeneDruhyZvireteProDanyLek as $druh) {
            $arrDruhu[$druh->ID_druhu] = $druh->nazev;
        }
        $form->addSelect('ID_druhu', 'Druhy', $arrDruhu);

        $form->addSubmit('send', 'Vložit do databáze')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'pridejDruhKLekuFormSucceeded');

        return $form;
    }

    /*
     * Akce pro vkládání/úpravu druhu u léku
     * @param  form    Formulář LekKeDruhuForm
     * @param  values  Hodnoty zadané ve formuláři
     */
    public function pridejDruhKLekuFormSucceeded($form, $values)
    {    
        // pokud nebyla hodnota zadána, nastaví se na NULL
        foreach ($values as & $value) if ($value === '' || $value === 0) $value=NULL;
        
        $ID_leku = $this->getParameter('ID_leku');
        $ID_druhu = $this->getParameter('ID_druhu');
        
        try {
             // pokud se jedná o úpravu přidání druhu k léku
            if ($ID_leku && $ID_druhu) {
                $druh_lek = $this->database->table('druh_lek')->where('ID_leku = ? AND ID_druhu = ?',$ID_leku, $ID_druhu)->fetch();
                $druh_lek->update($values);
            }
            // pokud se jedná o vložení nového druhu k danému léku
            else {
                $this->database->table('druh_lek')->insert($values);
            }
        
            $this->flashMessage('Data úspěšně uložena', 'alert-box success');
            $this->redirect('default', array('podleIdLeku' => $values->ID_leku));
        }
        catch (\Exception $e) {
            if ($e instanceof \Nette\Application\AbortException) {
                throw $e;
            }
            $this->flashMessage('Chyba: Druh zvířete nebyl přidán k danému léku', 'alert-box alert');
            $this->redirect('Lek:default');
        }
    }
    
}
