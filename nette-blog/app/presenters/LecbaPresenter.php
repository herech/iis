<?php

namespace App\Presenters;

use Nette;
use App\Model;
use Nette\Application\UI\Form;
use Tracy\Debugger;

/*
 * Prezenter pro práci s nemocemi
 */
class LecbaPresenter extends BasePresenter
{
    private $database;

    /*
     * Při startu se zkontroluje, zda je uživatel přihlášen
     */
    protected function startup()
    {
        parent::startup();

        if (!$this->user->isLoggedIn()) {
            if ($this->user->logoutReason === Nette\Security\IUserStorage::INACTIVITY) {
                $this->flashMessage('Byli jste odhlášeni kvůli své neaktivitě. Prosím, přihlašte se znovu.', 'alert-box info');
            }
            $this->redirect('Sign:in');
        }
    }

    /*
     * Prezenteru se předá databáze
     */
    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    /*
     * Připraví se léčby pro zobrazení
     */
    public function renderDefault()
    {
        $zobraz_souvisejici_lecby_podle_id_zvirete = $this->getParameter('zobrazSouvisejiciLecbyPodleIdZvirete');
        $this->template->vyhledej_lecby = false;

        // pokud chceme zobrazit léčby pro určité zvíře
        if ($zobraz_souvisejici_lecby_podle_id_zvirete) {
            $this->template->zobraz_souvisejici_lecby_podle_id_zvirete = true;
            $this->template->zvire = $this->database->table('zvire')->get($zobraz_souvisejici_lecby_podle_id_zvirete);
            $this->template->lecby = $this->database->table('lecba')->where('ID_zvirete', $zobraz_souvisejici_lecby_podle_id_zvirete)->order('poradove_cislo_lecby');
        }
        // pokud nechceme zobrazit léčby pro určité zvíře
        else {
            $this->template->zobraz_souvisejici_lecby_podle_id_zvirete = false;

            // pokud chceme vyhledat určitou léčbu
            if ($this->getParameter('send') && ($this->getParameter('zvire') || $this->getParameter('nemoc')) || $this->getParameter('stav')) {
                   
                    $this->template->vyhledej_lecby = true;
                    $selection = $this->database->table('lecba');

                    // uživatel mohl hledat podle více kritérií, my v databázi vyhledáme záznam jen pro vyplněné kritéria (např. léčby podle léčené nemoci) 
                    $searchParams = '';
                    foreach($this->request->getParameters() as $key => $value) {
                        if ($key == 'zvire' || $key == 'nemoc' || $key == 'stav') {
                            if (!empty($value)) {
                                $searchParams .= $key . '='. $value . ', ';

                                if ($key == 'zvire') {
                                    $key = $key . '.jmeno';
                                }
                                elseif ($key == 'nemoc') {
                                    $key = $key . '.nazev';
                                }

                                $selection = $selection->where($key, $value);
                            }
                        }
                    }
                    $this->template->lecby = $selection->order('ID_zvirete')->order('poradove_cislo_lecby');
                    $this->template->searchParams = substr($searchParams, 0, -2);
            }
            // pokud chceme zobrazit všechny léčby
            else {
                $this->template->lecby = $this->database->table('lecba')->order('ID_zvirete')->order('poradove_cislo_lecby');
            }
        }
    }
    
    /*
     * Funkce která kontroluje zobrazení stránky pro vytvořené nové léčby
     */
    public function renderCreate() {
        // léčbu může přidávat pouze doktor a výše
        if(!$this->getUser()->isInRole('Doktor')) {
            $this->flashMessage('Nemáte oprávnění vytvářet novou léčbu', 'alert-box alert');
            $this->redirect('default');
        }
    }

    /*
     * Formulář pro přidávání/úpravu léčby
     */
    protected function createComponentLecbaForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addHidden('poradove_cislo_lecby', $this->getParameter('poradove_cislo_lecby'));
        
        // zvířat se načtou z jiné tabulky
        $zvirata = $this->database->table('zvire')->order('jmeno');
        $arrZvirat = array();
        foreach ($zvirata as $zvire) {
            $arrZvirat[$zvire->ID_zvirete] = $zvire->jmeno.' ('.$zvire->majitel->jmeno.' '.$zvire->majitel->prijmeni.')';
        }
        $form->addSelect('ID_zvirete', 'Zvíře', $arrZvirat)
        ->setRequired('Prosím zvolte zvířet');

        // nemoci se načtou z jiné tabulky
        $nemoci = $this->database->table('nemoc')->order('nazev');
        $arrNemoci = array();
        foreach ($nemoci as $nemoc) {
            $arrNemoci[$nemoc->ID_nemoci] = $nemoc->nazev;
        }
        $form->addSelect('ID_nemoci', 'Nemoc', $arrNemoci)
        ->setRequired('Prosím zvolte nemoc');

        $form->addText('upresneni_diagnozy', 'Upřesnění diagnózy')
        ->setAttribute('placeholder', 'Upřesnění diagnózy');

        $form->addText('datum_zahajeni_lecby', 'Datum zahájení léčby')
        ->setDefaultValue(date('d.m.Y'))
        ->addCondition(Form::FILLED)
        ->addFilter(function ($value) {
            return preg_replace('@^(\d{2})\\.(\d{2})\\.(\d{4})$@', '$3-$2-$1', $value);
        });

        $enum = array('započatá' => 'započatá','dokončená' => 'dokončená');
        $form->addRadioList('stav', 'Stav', $enum)
        ->setDefaultValue('započatá');

        $form->addText('cena', 'Cena')
        ->setAttribute('placeholder', 'Cena léčby v Kč (jednotky nevkládat)')
        ->setAttribute('maxlength', 10)
        ->setDefaultValue( NULL )
        ->addCondition(Form::FILLED)
        ->addRule(Form::RANGE, 'Cena léčby musí být nezáporné celé číslo', array(0, null))
        ->addRule(Form::MAX_LENGTH, 'Tak vysokou cenu není možné žádat !!!', 10);

        // zaměstnanec který provedl diagnózu se načte z jiné tabulky
        $zamestnanec = $this->database->table('zamestnanec')->where('ID_zamestnance', $this->user->id)->fetch();
        $form->addText('vypis_zamestnance', 'Diagnózu provedl')
        ->setAttribute('placeholder', $zamestnanec->jmeno.' '.$zamestnanec->prijmeni)
        ->setDisabled();

        // skryté pole podle kterého poznáme který zaměstnanec provedl diagnózu
        $form->addHidden('ID_zamestnance', $zamestnanec->ID_zamestnance);

        $form->addSubmit('send', 'Vložit do databáze')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'lecbaFormSucceeded');

        return $form;
    }

    /*
     * Akce pro úpravu léčby
     * @param  $ID_zvirete  ID zvířete na nějž se zvtahuje léčba
     * @param  $poradove_cislo_lecby  pořadové číslo léčby pro dané zvíře
     */
    public function actionEdit($ID_zvirete, $poradove_cislo_lecby)
    {
        // léčbu muže upravovat pouze doktor
        if(!$this->getUser()->isInRole('Doktor')) {
            $this->flashMessage('Nemáte oprávnění editovat danou léčbu', 'alert-box alert');
            $this->redirect('default');
        }
    
        $lecba = $this->database->table('lecba')->where('poradove_cislo_lecby = ? AND ID_zvirete = ?', $poradove_cislo_lecby, $ID_zvirete)->fetch();
        // pokud léčba s danými parametry nebyla nalezena
        if (!$lecba) {
            $this->flashMessage('Léčba nebyla nalezena', 'alert-box alert');
            $this->redirect('default');
        }

        $arrLecba = $lecba->toArray();

        // upravíme formát data léčby z databázového do lidské podoby a případně doplníme dnešní datum, pokud není zadané
        if ($lecba['datum_zahajeni_lecby'] != NULL) {
            $arrLecba['datum_zahajeni_lecby'] = $lecba['datum_zahajeni_lecby']->format('d.m.Y');
        }
        else {
            $arrLecba['datum_zahajeni_lecby'] = date('d.m.Y');
        }

        // ve formuláři na úpravu léčby přidáme hodnoty z db, které uživatel upravuje
        $this['lecbaForm']['ID_zvirete']->setItems(array($lecba->ID_zvirete => $lecba->zvire->jmeno.' ('.$lecba->zvire->majitel->jmeno.' '.$lecba->zvire->majitel->prijmeni.')'));
        $this['lecbaForm']['ID_nemoci']->setItems(array($lecba->ID_nemoci => $lecba->nemoc->nazev));

        $this['lecbaForm']->setDefaults($arrLecba);

        $this['lecbaForm']['ID_zvirete']->setDisabled();
        $this['lecbaForm']['ID_nemoci']->setDisabled();

        $this['lecbaForm']['ID_zamestnance']->setValue($this->user->id);
    }

    /*
     * Akce pro smazání léčby
    * @param  $ID_zvirete  ID zvířete na nějž se vztahuje léčba
     * @param  $poradove_cislo_lecby  pořadové číslo léčby pro dané zvíře
     */
    public function actionDelete($ID_zvirete, $poradove_cislo_lecby)
    {
        // pouze doktor a výše může mazat léčbu
        if(!$this->getUser()->isInRole('Doktor')) {
            $this->flashMessage('Nemáte oprávnění mazat danou léčbu', 'alert-box alert');
            $this->redirect('default');
        }
    
        $lecba = $this->database->table('lecba')->where('poradove_cislo_lecby = ? AND ID_zvirete = ?', $poradove_cislo_lecby, $ID_zvirete)->fetch();
        // pokud léčba s danými parametry nebyla nalezena
        if (!$lecba) {
            $this->flashMessage('Léčba nebyla nalezena', 'alert-box alert');
            $this->redirect('default');
        }
        // smažeme léčbu
        else {
            $lecba->delete();
            
            $this->flashMessage('Léčba byla smazána', 'alert-box success');
            $this->redirect('default');
        }
    }

    /*
     * Akce pro vkládání/úpravu léčby
     * @param  form    Formulář LecbaForm
     * @param  values  Hodnoty zadané ve formuláři
     */
    public function lecbaFormSucceeded($form, $values)
    {
        // pouze doktor a výše může vkládat nebo upravovoat léčbu
        if(!$this->getUser()->isInRole('Doktor')) {
            $this->flashMessage('Nemáte oprávnění vytvářet novou léčbu', 'alert-box alert');
            $this->redirect('default');
        }

        $doslo_k_chybe = false;
        $ID_zvirete = $this->getParameter('ID_zvirete');
        $poradove_cislo_lecby = $this->getParameter('poradove_cislo_lecby');
        
        // pokud nebyla hodnota zadána, nastaví se na NULL
        foreach ($values as $key => $value) if ($value === '') unset($values[$key]);

        try {
            // zkontrolujeme jestli je zadané datum léčby
            if (isset($values['datum_zahajeni_lecby'])) {
                // pokud je zadané v nesprávnm formátu
                if (!preg_match('@^(\d{4})-(\d{2})-(\d{2})$@',$values['datum_zahajeni_lecby'], $matches)) {
                    $form->addError('Datum zahájení léčby není ve správném formátu');
                    $doslo_k_chybe = true;
                }
                // pokud je zadáno ve správném formátu
                else {
                    $year = $matches[1];
                    $day = $matches[3];
                    $month = $matches[2];
                    // zkotrolujeme jestli sedí i sémanticky, nejen formátově
                    if (!checkdate ( $month , $day , $year )) {
                        $form->addError('Datum zahájení léčby není ve správném formátu');
                        $doslo_k_chybe = true;
                    }
                }
            }
            // pokud byly zadané hodnoty v pořádku
            if ($doslo_k_chybe == false) {
                // pokud se jedná o úpravu léčby
                if ($ID_zvirete && $poradove_cislo_lecby) {
                    $lecba = $this->database->table('lecba')->where('poradove_cislo_lecby = ? AND ID_zvirete = ?', $poradove_cislo_lecby, $ID_zvirete)->fetch();
                    $lecba->update($values);
                }
                // pokud se jedná o vožení nové léčby
                else {
                    unset($values->poradove_cislo_lecby);

                    $lecba = $this->database->table('lecba')->insert($values);
                }

                $this->flashMessage('Data úspěšně uložena', 'alert-box success');
                $this->redirect('default');
            }
        }
        catch (\Exception $e) {
            if ($e instanceof \Nette\Application\AbortException) {
                throw $e;
            }
            $this->flashMessage('Chyba: Data nemohla být uložena', 'alert-box alert');
            $this->redirect('default');
        }
    }

    /*
     * Formulář pro vyhledání léčby
     */
    protected function createComponentSearchLecbaForm()
    {
        $form = new Nette\Application\UI\Form;
        $form->setAction($this->link('default'));
        $form->setMethod('GET');

        $form->addText('zvire', 'Jméno')
        ->setAttribute('placeholder', 'Jméno zvířete');

        $form->addText('nemoc', 'Název')
        ->setAttribute('placeholder', 'Název nemoci');

        $form->addText('stav', 'Stav')
        ->setAttribute('placeholder', 'Stav léčby');

        $form->addSubmit('send', 'Vyhledej')->setAttribute('class', 'button postfix');

        return $form;
    }
}
