<?php

namespace App\Presenters;

use Nette;
use App\Model;
use Nette\Application\UI\Form;
use Tracy\Debugger;

/*
 * Prezenter pro práci s nemocemi
 */
class NemocPresenter extends BasePresenter
{
    private $database;

    /*
     * Při startu se zkontroluje, zda je uživatel přihlášen
     */
    protected function startup()
    {
        parent::startup();

        if (!$this->user->isLoggedIn()) {
            if ($this->user->logoutReason === Nette\Security\IUserStorage::INACTIVITY) {
                $this->flashMessage('Byli jste odhlášeni kvůli své neaktivitě. Prosím, přihlašte se znovu.', 'alert-box info');
            }
            $this->redirect('Sign:in');
        }
    }

    /*
     * Prezenteru se předá databáze
     */
    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    /*
     * Připraví se nemoci pro zobrazení
     */
    public function renderDefault()
    {
        $this->template->vyhledej_nemoci = false;

        // pokud chceme vyhledat nemoc podne nějakého kritéria
        if ($this->getParameter('send') && ($this->getParameter('nazev') || $this->getParameter('priznaky'))) {
            // vyhledávali jsme nemoci
            $this->template->vyhledej_nemoci = true;
            // vybereme všechny nemoci
            $selection = $this->database->table('nemoc');

            // připravíme řetězec vyhledávacích kritérií
            $searchParams = '';

            // procházíme předaná vyhledávací kritéria
            foreach($this->request->getParameters() as $key => $value) {
                // je to správné kritérium
                if ($key == 'nazev' || $key == 'priznaky') {
                    // je zadána požadovaná hodnota
                    if (!empty($value)) {
                        // přidáme kritérium do řetězce
                        $searchParams .= $key . '='. $value . ', ';

                        // z nemocí odebereme ty, které nevyhovují danému kritériu
                        $selection = $selection->where($key, $value);
                    }
                }
            }

            // předáme vyhledané nemoci
            $this->template->nemoci = $selection->order('nazev');
            // předáme řetězec kritérií bez ", " na konci
            $this->template->searchParams = substr($searchParams, 0, -2);
        }
        else { // nevyhledáváme nemoci
            // předáme všechny nemoci
            $this->template->nemoci = $this->database->table('nemoc')->order('nazev');
        }
    }

    /*
     * Formulář pro přidávání/úpravu nemocí
     */
    protected function createComponentNemocForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addText('nazev', 'Název nemoci')
        ->setRequired('Prosím vyplňte název nemoci');

        $form->addText('priznaky', 'Příznaky');

        $form->addSubmit('send', 'Vložit do databáze')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'nemocFormSucceeded');

        return $form;
    }

    /*
     * Akce pro úpravu nemocí
     * @param  $ID_nemoci  ID upravované nemoci
     */
    public function actionEdit($ID_nemoci)
    {
        try {
            // z databáze si vytáhneme záznam upravované nemoci
            $nemoc = $this->database->table('nemoc')->get($ID_nemoci);

            // pokud tento záznam neexistuje, vypíšeme chybovou hlášku a vrátíme se na výchozí presenter pro nemoc
            if (!$nemoc) {
                $this->flashMessage('Nemoc nebyla nalezena', 'alert-box alert');
                $this->redirect('default');
            }

            // pokud existuje, nastavíme jeho hodnoty do formuláře pro úpravu nemoci jako výchozí
            $this['nemocForm']->setDefaults($nemoc->toArray());
        }
        catch (\Exception $e) {
            if ($e instanceof \Nette\Application\AbortException) {
                throw $e;
            }
            $this->flashMessage('Chyba: Akce editace nemohla být provedena', 'alert-box alert');
            $this->redirect('default');
        }
    }

    /*
     * Akce pro smazání nemoci
     * @param  $ID_nemoci  ID mazané nemoci
     */
    public function actionDelete($ID_nemoci)
    {
        try {
            // z databáze si vytáhneme záznam mazané nemoci
            $nemoc = $this->database->table('nemoc')->get($ID_nemoci);

            if (!$nemoc) { // pokud neexistuje, vypíšeme chybovou hlášku a vrátíme se na výchozí presenter
                $this->flashMessage('Nemoc nebyla nalezena', 'alert-box alert');
                $this->redirect('default');
            }
            else { // pokud existuje, smažeme ho, vypíšeme hlášku o úspěchu a vrátíme se na výchozí presenter
                $nemoc->delete();

                $this->flashMessage('Nemoc byla smazána', 'alert-box success');
                $this->redirect('default');
            }
        }
        catch (\Exception $e) {
            if ($e instanceof \Nette\Application\AbortException) {
                throw $e;
            }
            $this->flashMessage('Chyba: Akce smazání nemohla být provedena', 'alert-box alert');
            $this->redirect('default');
        }
    }

    /*
     * Akce pro vkládání/úpravu nemoci
     * @param  form    Formulář NemocForm
     * @param  values  Hodnoty zadané ve formuláři
     */
    public function nemocFormSucceeded($form, $values)
    {
        $ID_nemoci = $this->getParameter('ID_nemoci');

        // pokud nebyla hodnota zadána, nastaví se na NULL
        foreach ($values as & $value) if ($value === '' || $value === 0) $value = NULL;

        if ($ID_nemoci) { // pokud bylo zadáno ID_nemoci, budeme upravovat
            // z databáze si vytáhneme záznam upravované nemoci
            $nemoc = $this->database->table('nemoc')->get($ID_nemoci);

            // pokud tento záznam neexistuje, vypíšeme chybovou hlášku a vrátíme se na výchozí presenter pro nemoc
            if (!$nemoc) {
                $this->flashMessage('Nemoc nebyla nalezena', 'alert-box alert');
                $this->redirect('default');
            }

            // aktualizujeme záznam
            $nemoc->update($values);
        }
        else { // jinak budeme vkládat
            $nemoc = $this->database->table('nemoc')->insert($values);
        }

        // nemoc úspěšně vložena/upravena, vypíšeme hlášku o úspěchu a vrátíme se na výchozí presenter
        $this->flashMessage('Data úspěšně uložena', 'alert-box success');
        $this->redirect('default');
    }

    /*
     * Formulář pro vyhledávání zvířete
     */
    protected function createComponentSearchNemocForm()
    {
        $form = new Nette\Application\UI\Form;

        // při odeslání formuláře se přesměrujeme na prezenster Nemoc:default
        // a tam vypíšeme nemoci podle zvolených kritérií
        $form->setAction($this->link('default'));
        $form->setMethod('GET');

        $form->addText('nazev', 'Název')
        ->setAttribute('placeholder', 'Název nemoci');

        $form->addText('priznaky', 'Příznaky')
        ->setAttribute('placeholder', 'Příznaky nemoci');

        $form->addSubmit('send', 'Vyhledej')->setAttribute('class', 'button postfix');

        return $form;
    }
}
