<?php

namespace App\Presenters;

use Nette;
use App\Model;

/*
 * Prezenter pro přihlášení uživatelů do systému, uživatel je do něj vždy přesměrován pokud není přihlášen
 */
class SignPresenter extends BasePresenter
{

	/** @persistent */
    	//public $backlink = '';

    /*
     * Formulář pro zadání přihlašovacích údajů
     */
	protected function createComponentSignInForm()
	{
	    $form = new Nette\Application\UI\Form;
	    $form->addText('login', 'Login')
		->setRequired('Prosím vyplňte svůj login.')->setAttribute('placeholder', 'Váš login');

	    $form->addPassword('password', 'Heslo')
		->setRequired('Prosím vyplňte své heslo.')->setAttribute('placeholder', 'Vaše heslo');

	    $form->addSubmit('send', 'Přihlásit se')->setAttribute('class', 'button');

	    $form->onSuccess[] = array($this, 'signInFormSucceeded');
	    return $form;
	}

    /*
     * Akce pro přihlášení
     * @param  form    Formulář SignInForm
     */
	public function signInFormSucceeded($form)
	{
	    $values = $form->values;
        
        // zkusíme uživatele na základě zadaných údajů autentizovat
	    try {
		$this->getUser()->login($values->login, $values->password);
		$this->getUser()->setExpiration('15 minutes', TRUE);
		//$this->restoreRequest($this->backlink);
		$this->redirect('Homepage:default');

	    } 
        // autentizace se nezdarřila
        catch (Nette\Security\AuthenticationException $e) {
		$form->addError('Nesprávné přihlašovací jméno nebo heslo.');
	    }
	}
    /*
     * Akce pro odhlášení uživatele
     */
	public function actionOut()
	{
		$this->getUser()->logout();
		$this->flashMessage('Byli jste odhlášeni.', 'alert-box info');
		$this->redirect('in');
	}

	public function renderIn()
	{
		$this->template->anyVariable = 'any value';
	}

}
