<?php

namespace App\Presenters;

use Nette;
use App\Model;
use Nette\Application\UI\Form;
use Tracy\Debugger;

/*
 * Prezenter pro práci s léky
 */
class LekPresenter extends BasePresenter
{
    private $database;

    /*
     * Při startu se zkontroluje, zda je uživatel přihlášen
     */
    protected function startup()
    {
        parent::startup();

        if (!$this->user->isLoggedIn()) {
            if ($this->user->logoutReason === Nette\Security\IUserStorage::INACTIVITY) {
                $this->flashMessage('Byli jste odhlášeni kvůli své neaktivitě. Prosím, přihlašte se znovu.', 'alert-box info');
            }
            $this->redirect('Sign:in');
        }
    }

    /*
     * Prezenteru se předá databáze
     */
    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    /*
     * Připraví se léky pro zobrazení
     */
    public function renderDefault()
    {
        $this->template->vyhledej_lek = false;

        // pokud chceme vyhledat lék podne nějakého kritéria
        if ($this->getParameter('send') && ($this->getParameter('nazev') || $this->getParameter('typ'))) {
            // vyhledávali jsme lék
            $this->template->vyhledej_lek = true;
            // vybereme všechny léky
            $selection = $this->database->table('lek');

            // připravíme řetězec vyhledávacích kritérií
            $searchParams = "";

            // procházíme předaná vyhledávací kritéria
            foreach($this->request->getParameters() as $key => $value) {
                // je to správné kritérium
                if ($key == "nazev" || $key == "typ") {
                    // je zadána požadovaná hodnota
                    if (!empty($value)) {
                        // přidáme kritérium do řetězce
                        $searchParams .= $key . "=". $value . ", ";

                        // z léků odebereme ty, které nevyhovují danému kritériu
                        $selection = $selection->where($key, $value);
                    }
                }
            }

            // předáme vyhledané léky
            $this->template->leky = $selection->order('nazev');
            // předáme řetězec kritérií bez ", " na konci
            $this->template->searchParams = substr($searchParams, 0, -2);
        }
        else {  // nevyhledáváme léky
            // předáme všechny léky
            $this->template->leky = $this->database->table('lek')->order('nazev');
        }
    }

    /*
     * Formulář pro přidávání/úpravu léku
     */
    protected function createComponentLekForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addText('nazev', 'Název léku')
        ->setRequired('Prosím vyplňte název léku');

        // výčet pro výběr typu léku
        $enum = array('mast' => 'mast','tablety' => 'tablety','kapky' => 'kapky','čípek' => 'čípek', 'jiný' => 'jiný');
        $form->addRadioList('typ', 'Typ', $enum)
        ->setDefaultValue('mast');

        $form->addText('ucinna_latka', 'Účinná látka');

        $form->addText('kontraindikace', 'Kontraindikace');

        $form->addSubmit('send', 'Vložit do databáze')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'lekFormSucceeded');

        return $form;
    }

    /*
     * Akce pro úpravu léku
     * @param  $ID_leku  ID upravovaného léku
     */
    public function actionEdit($ID_leku)
    {
        // z databáze si vytáhneme záznam upravovaného léku
        $lek = $this->database->table('lek')->get($ID_leku);

        // pokud tento záznam neexistuje, vypíšeme chybovou hlášku a vrátíme se na výchozí presenter pro léky
        if (!$lek) {
            $this->flashMessage('Lék nebyl nalezen', 'alert-box alert');
            $this->redirect('default');
        }

        // pokud existuje, nastavíme jeho hodnoty do formuláře pro úpravu léku jako výchozí
        $this['lekForm']->setDefaults($lek->toArray());
    }


    /*
     * Akce pro smazání léku
     * @param  $ID_leku  ID mazaného léku
     */
    public function actionDelete($ID_leku)
    {
        // z databáze si vytáhneme záznam mazaného léku
        $lek = $this->database->table('lek')->get($ID_leku);

        if (!$lek) { // pokud neexistuje, vypíšeme chybovou hlášku a vrátíme se na výchozí presenter
            $this->flashMessage('Lék nebyl nalezen', 'alert-box alert');
            $this->redirect('default');
        }
        else { // pokud existuje, smažeme ho, vypíšeme hlášku o úspěchu a vrátíme se na výchozí presenter
            $lek->delete();

            $this->flashMessage('Lék byl smazán', 'alert-box success');
            $this->redirect('default');
        }
    }

    /*
     * Akce pro vkládání/úpravu léku
     * @param  form    Formulář LekForm
     * @param  values  Hodnoty zadané ve formuláři
     */
    public function lekFormSucceeded($form, $values)
    {
        $ID_leku = $this->getParameter('ID_leku');

        // pokud nebyla hodnota zadána, nastaví se na NULL
        foreach ($values as & $value) if ($value === '' || $value === 0) $value = NULL;

        if ($ID_leku) {  // pokud bylo zadáno ID_leku, budeme upravovat
            // z databáze si vytáhneme záznam upravovaného léku
            $lek = $this->database->table('lek')->get($ID_leku);

            // pokud tento záznam neexistuje, vypíšeme chybovou hlášku a vrátíme se na výchozí presenter
            if (!$lek) {
                $this->flashMessage('Lék nebyl nalezen', 'alert-box alert');
                $this->redirect('default');
            }

            // aktualizujeme záznam
            $lek->update($values);
        }
        else { // jinak budeme vkládat
            $lek = $this->database->table('lek')->insert($values);
        }

        // lék úspěšně vložen/upraven, vypíšeme hlášku o úspěchu a vrátíme se na výchozí presenter
        $this->flashMessage('Data úspěšně uložena', 'alert-box success');
        $this->redirect('default');
    }

    /*
     * Formulář pro vyhledávání léku
     */
    protected function createComponentSearchLekForm()
    {
        $form = new Nette\Application\UI\Form;

        // při odeslání formuláře se přesměrujeme na prezenster Lek:default
        // a tam vypíšeme léky podle zvolených kritérií
        $form->setAction($this->link('default'));
        $form->setMethod('GET');

        $form->addText('nazev', 'Název')
        ->setDefaultValue('')
        ->setAttribute('placeholder', 'Název léku');

        $form->addText('typ', 'Typ')
        ->setDefaultValue('')
        ->setAttribute('placeholder', 'Typ léku (mast, tablety, ...)');

        $form->addSubmit('send', 'Vyhledej')->setAttribute('class', 'button postfix');

        return $form;
    }
}
