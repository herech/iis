<?php

namespace App\Presenters;

use Nette;
use App\Model;
use Nette\Application\UI\Form;
use Tracy\Debugger;

/*
 * Prezenter pro práci s druhy zvířat
 */
class DruhPresenter extends BasePresenter
{
    private $database;

    /*
     * Při startu se zkontroluje, zda je uživatel přihlášen
     */
    protected function startup()
    {
        parent::startup();

        if (!$this->user->isLoggedIn()) {
            if ($this->user->logoutReason === Nette\Security\IUserStorage::INACTIVITY) {
                $this->flashMessage('You have been signed out due to inactivity. Please sign in again.', 'alert-box info');
            }
            $this->redirect('Sign:in');
        }
    }

    /*
     * Prezenteru se předá databáze
     */
    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    /*
     * Z datbáze se vyberou druhy pro zobrazení
     */
    public function renderDefault()
    {
        $this->template->druhy = $this->database->table('druh')->order('nazev');
    }

    /*
     * Formulář pro přidávání/úpravu druhu
     */
    protected function createComponentDruhForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addText('nazev', 'Název')
        ->setRequired('Prosím vyplňte název druhu')
        ->setAttribute('placeholder', 'Název druhu');

        $form->addSubmit('send', 'Vložit do databáze')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'druhFormSucceeded');

        return $form;
    }

    /*
     * Akce pro úpravu druhu
     * @param  $ID_druhu  ID upravovaného druhu
     */
    public function actionEdit($ID_druhu)
    {
        // z databáze si vytáhneme záznam upravovaného druhu
        $druh = $this->database->table('druh')->get($ID_druhu);

        // pokud tento záznam neexistuje, vypíšeme chybovou hlášku a vrátíme se na výchozí presenter pro druh
        if (!$druh) {
            $this->flashMessage('Druh nebyl nalezen', 'alert-box alert');
            $this->redirect('default');
        }

        // pokud existuje, nastavíme jeho hodnoty do formuláře pro úpravu druhu jako výchozí
        $this['druhForm']->setDefaults($druh->toArray());
    }

    /*
     * Akce pro smazání druhu
     * @param  $ID_druhu  ID mazaného druhu
     */
    public function actionDelete($ID_druhu)
    {
        // z databáze si vytáhneme záznam mazaného druhu
        $druh = $this->database->table('druh')->get($ID_druhu);

        if (!$druh) { // pokud neexistuje, vypíšeme chybovou hlášku a vrátíme se na výchozí presenter
            $this->flashMessage('Druh nebyl nalezen', 'alert-box alert');
            $this->redirect('default');
        }
        else { // pokud existuje, smažeme ho, vypíšeme hlášku o úspěchu a vrátíme se na výchozí presenter
            $druh->delete();

            $this->flashMessage('Druh byl smazán', 'alert-box success');
            $this->redirect('default');
        }
    }

    /*
     * Akce pro vkládání/úpravu druhu
     * @param  form    Formulář DruhForm
     * @param  values  Hodnoty zadané ve formuláři
     */
    public function druhFormSucceeded($form, $values)
    {
        $ID_druhu = $this->getParameter('ID_druhu');

        try {
        
            if ($ID_druhu) { // pokud bylo zadáno ID_druhu, budeme upravovat
                // z databáze si vytáhneme záznam upravovaného druhu
                $druh = $this->database->table('druh')->get($ID_druhu);

                // pokud tento záznam neexistuje, vypíšeme chybovou hlášku a vrátíme se na výchozí presenter
                if (!$druh) {
                    $this->flashMessage('Druh nebyl nalezen', 'alert-box alert');
                    $this->redirect('default');
                }

                // aktualizujeme záznam
                $druh->update($values);
            }
            else { // jinak budeme vkládat
                $druh = $this->database->table('druh')->insert($values);
            }

            // druh úspěšně vložen/upraven, vypíšeme hlášku o úspěchu a vrátíme se na výchozí presenter
            $this->flashMessage('Data úspěšně uložena', 'alert-box success');
            $this->redirect('default');
        }
        catch (\Exception $e) {
            if ($e instanceof \Nette\Application\AbortException) {
                throw $e;
            }
            $this->flashMessage('Chyba: Data nemohla být uložena', 'alert-box alert');
            $this->redirect('default');
        }
    }
}
