<?php

namespace App\Presenters;

use Nette;
use App\Model;
use Nette\Application\UI\Form;
use Tracy\Debugger;

/*
 * Prezenter pro práci s pozicemi zaměstnanců veterinární kliniky
 */
class PozicePresenter extends BasePresenter
{
    private $database;

    /*
     * Při startu se zkontroluje, zda je uživatel přihlášen a pokud je v roli vedoucího - nikdo jiný nemůže pozice spravovat
     */
    protected function startup()
    {
        parent::startup();

        if (!$this->user->isLoggedIn()) {
            if ($this->user->logoutReason === Nette\Security\IUserStorage::INACTIVITY) {
                $this->flashMessage('Byli jste odhlášeni kvůli své neaktivitě. Prosím, přihlašte se znovu.', 'alert-box info');
            }
            $this->redirect('Sign:in');
        }
        
        if(!$this->getUser()->isInRole('Vedouci')) {
            $this->flashMessage('Nemáte oprávnění přistupovat k pozicím', 'alert-box alert');
            $this->redirect('Homepage:default');
        }
    }
    
    /*
     * Prezenteru se předá databáze
     */
    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }
    
    /*
     * Připraví se pozice pro zobrazení - budou se vypisovat všechny pozice
     */
    public function renderDefault()
    {
        $this->template->pozice = $this->database->table('pozice')->order('nazev');
    }

    /*
     * Formulář pro přidávání/úpravu pozic
     */
    protected function createComponentPoziceForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addText('nazev', 'Název')
        ->setRequired('Prosím vyplňte název pozice')
        ->setAttribute('placeholder', 'Název pozice');
        
        $form->addText('hodinova_mzda', 'Hodinová mzda')
        ->setAttribute('placeholder', 'Hodinová mzda v Kč')
        ->setAttribute('maxlength', 10)
        ->setDefaultValue( NULL )
        ->addCondition(Form::FILLED)
        ->addRule(Form::RANGE, "Hodinvá mzda musí být nezáporné celé číslo", array(0, null))
        ->addRule(Form::MAX_LENGTH, 'Tak vysokou mzdu není možné vyplácet !!!', 10);

        $form->addSubmit('send', 'Vložit do databáze')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'poziceFormSucceeded');

        return $form;
    }

    /*
     * Akce pro úpravu pozic
     * @param  $ID_pozice  ID upravované pozice
     */
    public function actionEdit($ID_pozice)
    {
        $pozice = $this->database->table('pozice')->get($ID_pozice);
        
        // daná pozice nebyla nalezena - neplatné id pozice
        if (!$pozice) {
            $this->flashMessage('Pozice s daným ID nebyla nalezena', 'alert-box alert');
            $this->redirect('default');
        }
        // nastavíme ve formuláři jako výchozí hodnoty hodnoty z databáze pro danou pozici a uživatel je může upravovoat
        $this['poziceForm']->setDefaults($pozice->toArray());
    }
    
    /*
     * Akce pro smazání pozice
     * @param  $ID_pozice  ID mazané pozice
     */
    public function actionDelete($ID_pozice)
    {
        $pozice = $this->database->table('pozice')->get($ID_pozice);
    
        // daná pozice nebyla nalezena - neplatné id pozice
        if (!$pozice) {
            $this->flashMessage('Pozice s daným ID nebyla nalezena', 'alert-box alert');
            $this->redirect('default');
        }
        // smažeme pozici s daným id
        else {
            $pozice->delete();
            $this->flashMessage('Pozice byla smazána', 'alert-box success');
            $this->redirect('default');
        }
    }

    /*
     * Akce pro vkládání/úpravu zaměstnanců
     * @param  form    Formulář PoziceForm
     * @param  values  Hodnoty zadané ve formuláři
     */
    public function poziceFormSucceeded($form, $values)
    {    
    
        $ID_pozice = $this->getParameter('ID_pozice');

        try {
            // pokud nebyla hodnota zadána, nastaví se na NULL
            foreach ($values as & $value) if ($value === '' || $value === 0) $value=NULL;
                
                // jedná se o úpravu pozice       
                if ($ID_pozice) {
                    $pozice = $this->database->table('pozice')->get($ID_pozice);
                    $pozice->update($values);
                }
                // jedná seo vložení nové pozice
                else {
                    $pozice = $this->database->table('pozice')->insert($values);
                }

                $this->flashMessage('Data úspěšně uložena', 'alert-box success');
                $this->redirect('default');
        }
        catch (\Exception $e) {
            if ($e instanceof \Nette\Application\AbortException) {
                throw $e;
            }
            $this->flashMessage('Chyba: Data nemohla být uložena', 'alert-box alert');
            $this->redirect('default');
        }
    }
}
