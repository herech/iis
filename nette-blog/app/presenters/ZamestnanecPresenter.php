<?php

namespace App\Presenters;

use Nette;
use App\Model;
use Nette\Application\UI\Form;
use Tracy\Debugger;

/*
 * Prezenter pro práci se zaměstananci veterinární kliniky
 */
class ZamestnanecPresenter extends BasePresenter
{
    private $database;
    private $hledejZamestnance;

    /*
     * Při startu se zkontroluje, zda je uživatel přihlášen
     */
    protected function startup()
    {
        parent::startup();

        if (!$this->user->isLoggedIn()) {
            if ($this->user->logoutReason === Nette\Security\IUserStorage::INACTIVITY) {
                $this->flashMessage('Byli jste odhlášeni kvůli své neaktivitě. Prosím, přihlašte se znovu.', 'alert-box info');
            }
            $this->redirect('Sign:in');
        }
    }

    /*
     * Prezenteru se předá databáze
     */
    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
        $this->hledejZamestnance = false;
    }

    /*
     * Připraví se zaměstnanci pro zobrazení
     */
    public function renderDefault()
    {
    
        $this->template->zobraz_souvisejici_zamestnance_podle_id = false;
        $this->template->vyhledej_zamestnance = false;
        $this->template->zamestnanci = array();
        
        // pouze uživatelé s rolí doktor a výše si mohou zobrazovat jiné zaměstnance v systému
        if ($this->getUser()->isInRole('Doktor')) {
            
            // pokud chceme zobrazit pouze zaměstnance na určité pozici
            $zobraz_souvisejici_zamestnance_podle_id = $this->getParameter('zobrazSouvisejiciZamestnancePodleId');
            if ($zobraz_souvisejici_zamestnance_podle_id) {
                
                if ($this->getUser()->isInRole('Vedouci')) {
                    $this->template->zobraz_souvisejici_zamestnance_podle_id = true;
                    $this->template->pozice = $this->database->table('pozice')->get($zobraz_souvisejici_zamestnance_podle_id);
                    $this->template->zamestnanci = $this->database->table('zamestnanec')->where('ID_pozice', $zobraz_souvisejici_zamestnance_podle_id)->order('prijmeni')->order('jmeno');
                }
            }
            // pokud nechceme zobrazit zaměstnance pro určitou pozici
            else {
                // pokud chceme vyhledat a zobrazit jednoho určitého zaměstnance
                if ($this->getParameter('send') && ($this->getParameter('jmeno') || $this->getParameter('prijmeni')) || $this->getParameter('mesto')) {
                       
                        $this->template->vyhledej_zamestnance = true;
                        $selection = $this->database->table('zamestnanec');
                        
                        if (!$this->getUser()->isInRole('Vedouci')) {
                            $selection = $selection->where('pozice.nazev = ?', 'Sestra');
                        }
                        
                        // uživatel mohl hledat podle více kritérií, my v databázi vyhledáme záznam jen pro vyplněné kritéria (např. jméno)                                                                                                                                                            
                        $searchParams = "";
                        foreach($this->request->getParameters() as $key => $value) {
                            if ($key == "jmeno" || $key == "prijmeni" || $key == "mesto") {
                                if (!empty($value)) {
                                    $searchParams .= $key . "=". $value . ", ";
                                
                                    $selection = $selection->where($key, $value);
                                    
                                }
                            }
                        }
                        $this->template->zamestnanci = $selection->order('prijmeni')->order('jmeno');
                        $this->template->searchParams = substr($searchParams, 0, -2);
                }
                // pokud chceme vypsat všechny zaměstnance
                else {
                    $selection = $this->database->table('zamestnanec');
                    if (!$this->getUser()->isInRole('Vedouci')) {
                            $selection = $selection->where('pozice.nazev = ?', 'Sestra');
                    }
                
                    $this->template->zamestnanci = $selection->order('prijmeni')->order('jmeno');
                }
            }
        }
    }
    
    /*
     * Funkce která kontroluje zobrazení stránky pro vytvořené nového zaměstnance 
     */
    public function renderCreate()
    {
        // nového zaměstnnace může vytvářet pouze vědoucí
        if(!$this->getUser()->isInRole('Vedouci')) {
            $this->flashMessage('Nemáte oprávnění přidávat zaměstnance', 'alert-box alert');
            $this->redirect('default');
        }
        
    }
    /*
     * Funkce která nedělá nic, alespoň jedna taková v systému být vždy musí (easter egg pro opravující)
     */
    public function renderEdit()
    {
        
    }
    
    /*
     * Akce pro úpravu zaměstnance
     * @param  $ID_zamestnance  ID upravovaného zaměstnance
     */
    public function actionEdit($ID_zamestnance)
    {
        // jen vedoucí může upravovoat zaměstnance
        if(!$this->getUser()->isInRole('Vedouci')) {
            $this->flashMessage('Nemáte oprávnění upravovat zaměstnance (včetně sebe sama)', 'alert-box alert');
            $this->redirect('default');
        }
        
        $zamestnanec = $this->database->table('zamestnanec')->get($ID_zamestnance);
        // pokud zaměstnance s daným parametrem ID v databázi neexistuje
        if (!$zamestnanec) {
            //$this->error('Majitel nebyl nalezen');
            $this->flashMessage('Zaměstnanec nebyl nalezen', 'alert-box alert');
            $this->redirect('default');
        }
        // naszavíme ve formuláři pro úpravu zaměstnance výchozí hodnoty
        $this['zamestnanecForm']->setDefaults($zamestnanec->toArray());
    }
    
    /*
     * Akce pro smazání zaměstnance
     * @param  $ID_zamestnance  ID mazaného zaměstnance
     */
    public function actionDelete($ID_zamestnance)
    {
        // mazat zaměstnance může jen vedoucí
        if(!$this->getUser()->isInRole('Vedouci')) {
            $this->flashMessage('Nemáte oprávnění mazat zaměstnance (včetně sebe sama)', 'alert-box alert');
            $this->redirect('default');
        }
    
        $zamestnanec = $this->database->table('zamestnanec')->get($ID_zamestnance);
        
        // pokud daný zaměstnance v db neexistuje
        if (!$zamestnanec) {
            //$this->error('Majitel nebyl nalezen');
            $this->flashMessage('Zaměstnanec nebyl nalezen', 'alert-box alert');
            $this->redirect('default');
        }
        // pokud existuje smažeme jej
        else {
        $zamestnanec->delete();
        
        $this->flashMessage('Zaměstnanec byl smazán.', 'alert-box success');
        $this->redirect('default');
        }
    }
    
    /*
     * Formulář pro přidávání/úpravu zaměstnance
     */
    protected function createComponentZamestnanecForm()
    {
        $form = new Nette\Application\UI\Form;
        $form->addText('jmeno', 'Jméno')
        ->setRequired('Prosím vyplňte své jméno.')->setAttribute('placeholder', 'Vaše jméno');

        $form->addText('prijmeni', 'Příjmení')
        ->setRequired('Prosím vyplňte své příjmení.')->setAttribute('placeholder', 'Vaše příjmení');
        
        // pozice se načte z jiné tabulky
        $pozice = $this->database->table('pozice');
        $pole_pozic = array();
        foreach ($pozice as $urcita_pozice) {
            $pole_pozic[$urcita_pozice->ID_pozice] = $urcita_pozice->nazev;
        }
        
        $form->addSelect('ID_pozice', 'Pozice', $pole_pozic);

        $form->addText('titul', 'Titul')
        ->setAttribute('placeholder', 'Titul')
        ->setDefaultValue( NULL );
        
        $form->addText('ulice', 'Ulice')
        ->setAttribute('placeholder', 'Ulice')
        ->setDefaultValue( NULL );

        $form->addText('mesto', 'Město')
        ->setAttribute('placeholder', 'Město')
        ->setDefaultValue( NULL );

        $form->addText('psc', 'Poštovní směrovací číslo')
        ->setAttribute('placeholder', 'PSČ')
        ->setAttribute('maxlength', 5)
        ->setDefaultValue( NULL )
        ->addCondition(Form::FILLED)
        ->addRule(Form::RANGE, "PSČ musí být nezáporné celé číslo", array(0, null))
        ->addRule(Form::LENGTH, 'PSČ má 5 čísel!', 5);

        $form->addText('cislo_uctu', 'Číslo účtu')
        ->setAttribute('placeholder', 'Vaše číslo účtu')
        ->setAttribute('maxlength', 14)
        ->setDefaultValue( NULL )
        ->addCondition(Form::FILLED)
        ->addRule(Form::RANGE, "Číslo účtu musí být nezáporné celé číslo", array(0, null))
        ->addRule(Form::MIN_LENGTH, 'Číslo účtu je příliš krátké!', 10)
        ->addRule(Form::MAX_LENGTH, 'Číslo účtu je příliš dlouhé!', 14);
        
        $form->addText('login', 'Login')
        ->setRequired('Prosím vyplňte svůj login')->setAttribute('placeholder', 'Váš login');
        
        $form->addPassword('heslo', 'Heslo')
        ->setAttribute('placeholder', 'Vaše heslo (alespoň pětimístné)')
        ->addCondition(Form::FILLED)
        ->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň 5 znaků', 5);
        
        $form->addPassword('heslo_overeni', 'Heslo pro kontrolu')
        ->setAttribute('placeholder', 'Zadejte heslo znovu pro ověření')
        ->setOmitted(TRUE)
        ->addRule(Form::EQUAL, 'Hesla se neshodují', $form['heslo']);

        $form->addSubmit('send', 'Vložit do databáze')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'zamestnanecFormSucceeded');

        return $form;
    }
    
    /*
     * Akce pro vkládání/úpravu zaměstnanců
     * @param  form    Formulář ZamestnanecForm
     * @param  values  Hodnoty zadané ve formuláři
     */
    public function zamestnanecFormSucceeded($form, array $values)
    {    
        // pouze vedoucí může vkládat/upravovoat zaměstnance
        if(!$this->getUser()->isInRole('Vedouci')) {
            $this->flashMessage('Nemáte oprávnění přidávat/upravovat zaměstnance (včetně sebe sama)', 'alert-box alert');
            $this->redirect('default');
        }
    
        $doslo_k_chybe = false;
        $ID_zamestnance = $this->getParameter('ID_zamestnance');
        
        // pokud nebyla hodnota zadána, nastaví se na NULL
        foreach ($values as & $value) if ($value === '' || $value === 0) $value=NULL;
        
        try {
            
            // pokud je jedná o úpravu zaměstnance
            if ($ID_zamestnance) {

                if ($values['heslo'] === "") {
                    unset($values['heslo']);
                }
                else {
                    $values['heslo'] = Nette\Security\Passwords::hash($values['heslo']);
                }
                
                $zamestnanec = $this->database->table('zamestnanec')->get($ID_zamestnance);
                $zamestnanec->update($values);
            } 
            // pokud se jedná o vložené nového zaměstnance do db
            else {
                // nebylo zadáno heslo, chyba
                if (empty($values['heslo'])) {
                    $form->addError('Zadejte prosím své heslo.');
                     $doslo_k_chybe = true;
                }
                else {
      
                $values['heslo'] = Nette\Security\Passwords::hash($values['heslo']);
                $zamestnanec = $this->database->table('zamestnanec')->insert($values);
                }
            }
            // nedošlo k chybě
            if ( $doslo_k_chybe == false) {
                $this->flashMessage('Data úspěšně uložena.', 'alert-box success');

                $this->redirect('default');
            }
        }
        catch (\Exception $e) {
            if ($e instanceof \Nette\Application\AbortException) {
                throw $e;
            }
            $this->flashMessage('Chyba: Data nemohla být uložena', 'alert-box alert');
            $this->redirect('default');
        }
    }
    /*
     * Formulář pro vyhledání zaměstnance
     */
    protected function createComponentSearchZamestnanecForm()
    {
        $form = new Nette\Application\UI\Form;
        $form->setAction($this->link('default'));
        $form->setMethod('GET');
        
        $form->addText('jmeno', 'Jméno')
        ->setDefaultValue('')
        ->setAttribute('placeholder', 'Jméno zaměstnance');

        $form->addText('prijmeni', 'Příjmení')
        ->setDefaultValue('')
        ->setAttribute('placeholder', 'Příjmení zaměstnance');
        
        $form->addText('mesto', 'Město')
        ->setDefaultValue('')
        ->setAttribute('placeholder', 'Město zaměstnance');

        $form->addSubmit('send', 'Vyhledej')->setAttribute('class', 'button postfix');

        return $form;
    }
    
}
