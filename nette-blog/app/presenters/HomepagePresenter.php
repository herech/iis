<?php

namespace App\Presenters;

use Nette;
use App\Model;

/*
 * Prezenter pro domovskou stránku
 */
class HomepagePresenter extends BasePresenter
{

    /*
     * Při startu se zkontroluje, zda je uživatel přihlášen
     */
	protected function startup()
	{
		parent::startup();

		if (!$this->user->isLoggedIn()) {
		    if ($this->user->logoutReason === Nette\Security\IUserStorage::INACTIVITY) {
		        $this->flashMessage('Byli jste odhlášeni kvůli své neaktivitě. Prosím, přihlašte se znovu.', 'alert-box info');
		    }
		    $this->redirect('Sign:in');
		}
	}

	public function renderDefault()
	{
		$this->template->anyVariable = 'any value';
	}

}
