<?php

namespace App\Presenters;

use Nette;
use App\Model;
use Nette\Application\UI\Form;
use Tracy\Debugger;

/*
 * Prezenter který pracuje s propojením léků a nemocí - pracuje nad tabulkou spojení mezi těmito entitami v DB
 */
class LekNemocPresenter extends BasePresenter
{
    private $database;
    
    // perzistentní parametry, které se předávají napříč presenterem
    
    /** @persistent */
    public $pridejLekKNemociPodleID;
    /** @persistent */
    public $pridejNemocKLekuPodleID;
    /** @persistent */
    public $zobrazSouvisejiciLekyPodleIdNemoci;
    /** @persistent */
    public $zobrazSouvisejiciNemociPodleIdLeku;

    private $lekyProDanouNemoc;
    private $nemociProDanyLek;

    /*
     * Při startu se zkontroluje, zda je uživatel přihlášen
     */
    protected function startup()
    {
        parent::startup();

        if (!$this->user->isLoggedIn()) {
            if ($this->user->logoutReason === Nette\Security\IUserStorage::INACTIVITY) {
                $this->flashMessage('Byli jste odhlášeni kvůli své neaktivitě. Prosím, přihlašte se znovu.', 'alert-box info');
            }
            $this->redirect('Sign:in');
        }
    }

    /*
     * Prezenteru se předá databáze
     */
    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }
    
    /*
     * Připraví se zobrazení léků, které odpovídají určité nemoci a vice versa
     */
    public function renderDefault()
    {   
        $this->template->zobrazSouvisejiciLekyPodleIdNemoci = false;
        $this->template->zobrazSouvisejiciNemociPodleIdLeku = false;

        $zobrazSouvisejiciLekyPodleIdNemoci = $this->getParameter('podleIdNemoci');
        if (!$zobrazSouvisejiciLekyPodleIdNemoci) {
            $zobrazSouvisejiciLekyPodleIdNemoci = $this->getParameter('zobrazSouvisejiciLekyPodleIdNemoci');
        }

        $zobrazSouvisejiciNemociPodleIdLeku = $this->getParameter('podleIdLeku');
        if (!$zobrazSouvisejiciNemociPodleIdLeku) {
            $zobrazSouvisejiciNemociPodleIdLeku = $this->getParameter('zobrazSouvisejiciNemociPodleIdLeku');
        }
        
        // nejdřív zkusíme zjistit jestli uživatel nechce hledat související léky pro určitou nemoc a pokud ano, připravíme k zobrazení dané léky
        if ($zobrazSouvisejiciLekyPodleIdNemoci) {
            $nemoc = $this->database->table('nemoc')->get($zobrazSouvisejiciLekyPodleIdNemoci);
            // nemoc nebyla podle daného id nalezena
            if (!$nemoc) {
                $this->flashMessage('Daná nemoc nebyla nalezena', 'alert-box alert');
                $this->redirect('default');
            }
            $this->template->nemoc = $nemoc;
            $this->template->zobrazSouvisejiciLekyPodleIdNemoci = true;
            $this->template->souvisejiciLekyPodleIdNemoci = $this->database->table('lek_nemoc')->where('ID_nemoci', $zobrazSouvisejiciLekyPodleIdNemoci);
        }
        // jinak zkusíme zjistit jestli uživatel nechce hledat související nemoci pro určitý lék a pokud ano, připravíme k zobrazení dané nemoci
        else if($zobrazSouvisejiciNemociPodleIdLeku) {
            $lek = $this->database->table('lek')->get($zobrazSouvisejiciNemociPodleIdLeku);
            // lék nebyl podle daného id nalezen
            if (!$lek) {
                $this->flashMessage('Daný lék nebyl nalezen', 'alert-box alert');
                $this->redirect('default');
            }
            $this->template->lek = $lek;
            $this->template->zobrazSouvisejiciNemociPodleIdLeku = true;
            $this->template->souvisejiciNemociPodleIdLeku = $this->database->table('lek_nemoc')->where('ID_leku', $zobrazSouvisejiciNemociPodleIdLeku);
        }
    }
    
    /*
     * Funkce která kontroluje zobrazení stránky pro přidání léku k nemoci a vice versa
     */
    public function renderCreate() {
        $this->template->pridejLekKNemociPodleID = false;
        $this->template->pridejNemocKLekuPodleID = false;

        $pridejLekKNemociPodleID = $this->getParameter('pridejLekKNemociPodleID');
        $pridejNemocKLekuPodleID = $this->getParameter('pridejNemocKLekuPodleID');

        // nejdřív zkusíme zjistit jestli uživatel nechce přidat léky pro určitou nemoc
        if ($pridejLekKNemociPodleID) {
            $nemoc = $this->database->table('nemoc')->get($pridejLekKNemociPodleID);
            // nemoc nebyla podle daného id nalezena
            if (!$nemoc) {
                $this->flashMessage('Daná nemoc nebyla nalezena', 'alert-box alert');
                $this->redirect('default');
            }
            $this->template->nemoc = $nemoc;
            $this->template->pridejLekKNemociPodleID = true;

            $this->pridejLekKNemociPodleID = $pridejLekKNemociPodleID;
        }
        // jinak zkusíme zjistit jestli uživatel nechce přidat nemoci pro určitý lék
        else if($pridejNemocKLekuPodleID) {
            $lek = $this->database->table('lek')->get($pridejNemocKLekuPodleID);
            // lék nebyl podle daného id nalezen
            if (!$lek) {
                $this->flashMessage('Daný lék nebyl nalezen', 'alert-box alert');
                $this->redirect('default');
            }
            $this->template->lek = $lek;
            $this->template->pridejNemocKLekuPodleID = true;

            $this->pridejNemocKLekuPodleID = $pridejNemocKLekuPodleID;
        }
    }

    /*
     * Akce pro úpravu léků pro danou nemoc nebo a vice versa
     * @param  $upravLekyUNemoci - 'true' pokud se pro nemoc s id $ID_nemoci budou upravovoat její léky
     *                           - 'false' pokud se pro lék s id $ID_leku budou upravovoat nemoci ke kterým přísluší
     * @param  $ID_leku  ID léku
     * @param  $ID_nemoci  ID nemoci
     */
    public function actionEdit($upravLekyUNemoci, $ID_leku, $ID_nemoci)
    {
		try {
            $lek_nemoc = $this->database->table('lek_nemoc')->where('ID_leku = ? AND ID_nemoci = ?',$ID_leku, $ID_nemoci)->fetch();

            // existuje nějaké propojení mezi daným lékem a danou nemocí?
            if ($lek_nemoc) {
                $nazev_leku = $this->database->table('lek')->get($ID_leku);
                $nazev_nemoci = $this->database->table('nemoc')->get($ID_nemoci);
                
                $arr_lek_nemoc = $lek_nemoc->toArray();
                // pokud se pro nemoc s id $ID_nemoci budou upravovoat její léky
                if ($upravLekyUNemoci == 'true') {
                    $this['pridejLekKNemociForm']['ID_leku']->setItems(array($ID_leku => $nazev_leku->nazev));
                    $this['pridejLekKNemociForm']->setDefaults($arr_lek_nemoc);
                    $this['pridejLekKNemociForm']['ID_leku']->setDisabled();
                }
                // pokud se pro lék s id $ID_leku budou upravovoat nemoci ke kterým přísluší
                else if ($upravLekyUNemoci == 'false') {
                    $this['pridejNemocKLekuForm']['ID_nemoci']->setItems(array($ID_nemoci => $nazev_nemoci->nazev));
                    $this['pridejNemocKLekuForm']->setDefaults($arr_lek_nemoc);
                    $this['pridejNemocKLekuForm']['ID_nemoci']->setDisabled();
                }
            }
            // chyba, propojení neexistuje, není co upravovoat
            else {
                $this->flashMessage('Editace nemohla být provedena, zadané parametry jsou neplatné', 'alert-box alert');
                $this->redirect('default');
            }
		}
        catch (\Exception $e) {
            if ($e instanceof \Nette\Application\AbortException) {
                throw $e;
            }
            $this->flashMessage('Vyskytla se chyba: Editace nemohla být provedena', 'alert-box alert');
            $this->redirect('default');
        }
    }
    
    /*
     * Metoda pro přípravu vykreslení formuláře pro úpravu léků pro danou nemoc nebo a vice versa
     * @param  $upravLekyUNemoci - 'true' pokud se pro nemoc s id $ID_nemoci budou upravovoat její léky
     *                           - 'false' pokud se pro lék s id $ID_leku budou upravovoat nemoci ke kterým přísluší
     * @param  $ID_leku  ID léku
     * @param  $ID_nemoci  ID nemoci
     */
    public function renderEdit($upravLekyUNemoci, $ID_leku, $ID_nemoci)
    {
        $this->template->pridejLekKNemociPodleID = false;
        $this->template->pridejNemocKLekuPodleID = false;
        
        // pokud se pro nemoc s id $ID_nemoci budou upravovoat její léky
        if ($upravLekyUNemoci == 'true') {
            $nemoc = $this->database->table('nemoc')->get($ID_nemoci);

            $this->template->nemoc = $nemoc;
            $this->template->pridejLekKNemociPodleID = true;
            
            $this->pridejLekKNemociPodleID = $ID_nemoci;
            
        }
        // pokud se pro lék s id $ID_leku budou upravovoat nemoci ke kterým přísluší
        else if($upravLekyUNemoci == 'false') {
            $lek = $this->database->table('lek')->get($ID_leku);

            $this->template->lek = $lek;
            $this->template->pridejNemocKLekuPodleID = true;
            
            $this->pridejNemocKLekuPodleID = $ID_leku;
        }
    }

    /*
     * Akce pro smazání léku pro danou nemoc
     * @param  $ID_leku  ID léku
     * @param  $ID_nemoci  ID nemoci
     */
    public function actionDeleteLekUNemoci($ID_leku, $ID_nemoci)
    {
        try {
            $lek = $this->database->table('lek_nemoc')->where('ID_leku = ? AND ID_nemoci = ?',$ID_leku, $ID_nemoci);
            
            // pokud lék s daným id neexistuje pro danou nemoc
            if (!$lek) {
                $this->flashMessage('Lék nebyl nalezen', 'alert-box alert');
                $this->redirect('LekNemoc:default');
            }
            // smažeme lék pro danou nemoc
            else {
                $lek->delete();
                $this->flashMessage('Lék byl u dané nemoci smazán', 'alert-box success');
                $this->redirect('LekNemoc:default', array('podleIdNemoci' => $ID_nemoci));
            }
        }
        catch (\Exception $e) {
            if ($e instanceof \Nette\Application\AbortException) {
                throw $e;
            }
            $this->flashMessage('Vyskytla se chyba: akce pro smazání léku nemohla být provedena', 'alert-box alert');
            $this->redirect('default');
        }
    }

    /*
     * Akce pro smazání nemoci pro daný lék
     * @param  $ID_leku  ID léku
     * @param  $ID_nemoci  ID nemoci
     */
    public function actionDeleteNemocULeku($ID_leku, $ID_nemoci)
    {
        try {
            $nemoc = $this->database->table('lek_nemoc')->where('ID_leku = ? AND ID_nemoci = ?',$ID_leku, $ID_nemoci);
            
            // pokud nemoc s daným id neexistuje pro daný lék
            if (!$nemoc) {
                $this->flashMessage('Nemoc nebyla nalezena', 'alert-box alert');
                $this->redirect('LekNemoc:default');
            }
            // smažeme nemoc pro daný lék
            else {
                $nemoc->delete();
                $this->flashMessage('Nemoc byla u daného léku smazána', 'alert-box success');
                $this->redirect('LekNemoc:default', array('podleIdLeku' => $ID_leku));
            }
        }
        catch (\Exception $e) {
            if ($e instanceof \Nette\Application\AbortException) {
                throw $e;
            }
            $this->flashMessage('Vyskytla se chyba: akce pro smazání nemoci nemohla být provedena', 'alert-box alert');
            $this->redirect('default');
        }
    }
    
    /*
     * Formulář pro přidávání/úpravu léku k/u nemoci
     */
    protected function createComponentPridejLekKNemociForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addText('delka_lecby', 'Délka Léčby')
        ->setAttribute('placeholder', 'Délka Léčby');
        
        $form->addText('procento_uspesne_lecby', 'Procento úspěšné léčby')
        ->setAttribute('placeholder', 'Procento úspěšné léčby (znak procenta nevkládat)')
        ->setAttribute('maxlength', 5)
        ->addCondition(Form::FILLED)
        ->addRule(Form::FLOAT, 'Procento je číslo!')
        ->addRule(Form::RANGE, 'Procento musí být desetinné číslo menší než 100', array(0.0, 100.0));
        
        // pomocí skrytého pole rozliším nemoc ke které přidávám lék
        $form->addHidden('ID_nemoci', $this->getParameter('pridejLekKNemociPodleID'));

        // pro danou nemoc vytvoříme seznam léků, které jí ještě nebyly přiřazeny
            $tmpSelection = $this->database->table('lek_nemoc')->where('ID_nemoci',$this->pridejLekKNemociPodleID);
            $tmpSelection2 = $this->database->table('lek');
            $poleIDlekuVTabulce_lek_nemoc = array();
            $poleIDlekuVTabulce_lek = array();
            foreach($tmpSelection as $tmpSelectionn) {
                $poleIDlekuVTabulce_lek_nemoc[] = $tmpSelectionn->ID_leku;
            }
            foreach($tmpSelection2 as $tmpSelection2n) {
                $poleIDlekuVTabulce_lek[] = $tmpSelection2n->ID_leku;
            }
            
            $lekyProDanouNemoc = array_diff ( $poleIDlekuVTabulce_lek , $poleIDlekuVTabulce_lek_nemoc);
        
        $neprirazeneLekyProDanouNemoc = $this->database->table('lek')->where('ID_leku', $lekyProDanouNemoc);
        $arrLeku = array();
        foreach ($neprirazeneLekyProDanouNemoc as $lek) {
            $arrLeku[$lek->ID_leku] = $lek->nazev;
        }
        $form->addSelect('ID_leku', 'Léky', $arrLeku);

        $form->addSubmit('send', 'Vložit do databáze')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'pridejLekKNemociFormSucceeded');

        return $form;
    }

    /*
     * Formulář pro přidávání/úpravu nemoci k/u léku
     */
    protected function createComponentPridejNemocKLekuForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addText('delka_lecby', 'Délka Léčby')
        ->setAttribute('placeholder', 'Délka Léčby');
        
        $form->addText('procento_uspesne_lecby', 'Procento úspěšné léčby')
        ->setAttribute('placeholder', 'Procento úspěšné léčby (znak procenta nevkládat)')
        ->setAttribute('maxlength', 5)
        ->addCondition(Form::FILLED)
        ->addRule(Form::FLOAT, 'Procento je číslo!')
        ->addRule(Form::RANGE, 'Procento musí být desetinné číslo menší než 100', array(0.0, 100.0));
        
        // pomocí skrytého pole si uložím lék ke kterému přidávam nemoc
        $form->addHidden('ID_leku', $this->getParameter('pridejNemocKLekuPodleID'));

        // pro dané lék vytvoříme seznam nemocí, které mu ještě nebyly přiřazeny
            $tmpSelection = $this->database->table('lek_nemoc')->where('ID_leku',$this->pridejNemocKLekuPodleID);
            $tmpSelection2 = $this->database->table('nemoc');
            $poleIDnemociVTabulce_lek_nemoc = array();
            $poleIDnemociVTabulce_nemoc = array();
            foreach($tmpSelection as $tmpSelectionn) {
                $poleIDnemociVTabulce_lek_nemoc[] = $tmpSelectionn->ID_nemoci;
            }
            foreach($tmpSelection2 as $tmpSelection2n) {
                $poleIDnemociVTabulce_nemoc[] = $tmpSelection2n->ID_nemoci;
            }
            
            $nemociProDanyLek = array_diff ( $poleIDnemociVTabulce_nemoc , $poleIDnemociVTabulce_lek_nemoc);
        
        $neprirazeneNemociProDanyLek = $this->database->table('nemoc')->where('ID_nemoci', $nemociProDanyLek);
        $arrNemoci = array();
        foreach ($neprirazeneNemociProDanyLek as $nemoc) {
            $arrNemoci[$nemoc->ID_nemoci] = $nemoc->nazev;
        }
        $form->addSelect('ID_nemoci', 'Nemoci', $arrNemoci);

        $form->addSubmit('send', 'Vložit do databáze')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'pridejNemocKLekuFormSucceeded');

        return $form;
    }

    /*
     * Akce pro vkládání/úpravu léku u nemoci
     * @param  form    Formulář NemocKLekuForm
     * @param  values  Hodnoty zadané ve formuláři
     */
    public function pridejLekKNemociFormSucceeded($form, $values)
    {    
        // pokud nebyla hodnota zadána, nastaví se na NULL
        foreach ($values as & $value) if ($value === '' || $value === 0) $value=NULL;

        $ID_leku = $this->getParameter('ID_leku');
        $ID_nemoci = $this->getParameter('ID_nemoci');
        
        try {
            // pokud se jedná o úpravu přidání léku k nemoci
            if ($ID_leku && $ID_nemoci) {
                $lek_nemoc = $this->database->table('lek_nemoc')->where('ID_leku = ? AND ID_nemoci = ?',$ID_leku, $ID_nemoci)->fetch();
                $lek_nemoc->update($values);
            }
            // pokud se jedná o vložení léku k nemoci
            else {
                $this->database->table('lek_nemoc')->insert($values);
            }

            $this->flashMessage('Data úspěšně uložena', 'alert-box success');
            $this->redirect('LekNemoc:default', array('podleIdNemoci' => $values->ID_nemoci));

        }
        catch (\Exception $e) {
            if ($e instanceof \Nette\Application\AbortException) {
                throw $e;
            }
            $this->flashMessage('Chyba: Lék nebyl přidán k nemoci', 'alert-box alert');
            $this->redirect('Nemoc:default');
        }
    }

    /*
     * Akce pro vkládání/úpravu nemoci u léku
     * @param  form    Formulář NemocKLekuForm
     * @param  values  Hodnoty zadané ve formuláři
     */
    public function pridejNemocKLekuFormSucceeded($form, $values)
    {    
        // pokud nebyla hodnota zadána, nastaví se na NULL
        foreach ($values as & $value) if ($value === '' || $value === 0) $value=NULL;

        $ID_leku = $this->getParameter('ID_leku');
        $ID_nemoci = $this->getParameter('ID_nemoci');

        try {
            // pokud se jedná o úpravu přidání nemoci k léku
            if ($ID_leku && $ID_nemoci) {
                $lek_nemoc = $this->database->table('lek_nemoc')->where('ID_leku = ? AND ID_nemoci = ?',$ID_leku, $ID_nemoci)->fetch();
                $lek_nemoc->update($values);
            }
            // pokud se jedná o vložení nemoci k léku
            else {
                $this->database->table('lek_nemoc')->insert($values);
            }

            $this->flashMessage('Data úspěšně uložena', 'alert-box success');
            $this->redirect('LekNemoc:default', array('podleIdLeku' => $values->ID_leku));

        }
        catch (\Exception $e) {
            if ($e instanceof \Nette\Application\AbortException) {
                throw $e;
            }
            $this->flashMessage('Chyba: Nemoc nebyla přidána k léku', 'alert-box alert');
            $this->redirect('Lek:default');
        }
    }
}
