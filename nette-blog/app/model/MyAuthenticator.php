<?php
namespace App\Model;

use Nette\Security as NS;
use Nette;

/*
 * Model pro autentizaci uživatele
 */
class MyAuthenticator extends Nette\Object implements NS\IAuthenticator
{
    public $database;

    /*
     * Modelu se předá databáze
     */
    function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    /*
     * Metoda pro autentizaci uživatele
     */
    function authenticate(array $credentials)
    {
        // ověříme, jestli se v DB vyskytuje takový uživatel s takovým heslem
        list($username, $password) = $credentials;
        $row = $this->database->table('zamestnanec')
            ->where('login', $username)->fetch();

        // uživatel s daným loginem neexistuje
        if (!$row) {
            throw new NS\AuthenticationException('User not found.');
        }
        // uživatel existuje, ale zadal nesprávné heslo
        if (!NS\Passwords::verify($password, $row->heslo)) {
            throw new NS\AuthenticationException('Invalid password.');
        }
        
        // podle role v databázi určíme role v systému - tedy nadřazená role obsahuje všechny podřazené role (Doktor má roli Doktor a zároveň roli Sestra apod.)
        $role = $row->ref('pozice', 'ID_pozice')->nazev;
        if ($role == "Doktor") {
            $role = $role . " Sestra"; 
        }
        else if ($role == "Vedouci") {
            $role = $role . " Doktor Sestra"; 
        }
        // vytvoříme objekt identity uživatele
        return new NS\Identity($row->ID_zamestnance, explode(" ",$role), array('username' => $row->login));
    }
}
