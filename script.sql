-- phpMyAdmin SQL Dump
-- version 4.4.6.1
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Dec 07, 2015 at 07:16 PM
-- Server version: 5.6.27-log
-- PHP Version: 5.6.14-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `czveterinarni-klinika`
--

-- --------------------------------------------------------

--
-- Table structure for table `druh`
--

CREATE TABLE IF NOT EXISTS `druh` (
  `ID_druhu` int(10) unsigned NOT NULL,
  `nazev` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `druh`
--

INSERT INTO `druh` (`ID_druhu`, `nazev`) VALUES
(1, 'pes'),
(3, 'želva'),
(7, 'kočka');

-- --------------------------------------------------------

--
-- Table structure for table `druh_lek`
--

CREATE TABLE IF NOT EXISTS `druh_lek` (
  `ID_druhu` int(10) unsigned NOT NULL,
  `ID_leku` int(10) unsigned NOT NULL,
  `doporucene_davkovani` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vedlejsi_ucinky` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `druh_lek`
--

INSERT INTO `druh_lek` (`ID_druhu`, `ID_leku`, `doporucene_davkovani`, `vedlejsi_ucinky`) VALUES
(1, 1, 'jednu tabletu denně, při velkých obtížích dvě', NULL),
(1, 2, 'nanášet pravidelně po třech hodinách (v noci není potřeba)', NULL),
(1, 3, NULL, NULL),
(1, 4, NULL, NULL),
(3, 1, 'foo', 'želva ninja'),
(3, 2, NULL, NULL),
(3, 3, NULL, NULL),
(3, 4, NULL, NULL),
(3, 5, NULL, NULL),
(7, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lecba`
--

CREATE TABLE IF NOT EXISTS `lecba` (
  `poradove_cislo_lecby` int(10) unsigned NOT NULL,
  `ID_zamestnance` int(10) unsigned NOT NULL,
  `ID_zvirete` int(10) unsigned NOT NULL,
  `ID_nemoci` int(10) unsigned NOT NULL,
  `upresneni_diagnozy` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `datum_zahajeni_lecby` date DEFAULT NULL,
  `stav` enum('započatá','dokončená') COLLATE utf8mb4_unicode_ci NOT NULL,
  `cena` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lecba`
--

INSERT INTO `lecba` (`poradove_cislo_lecby`, `ID_zamestnance`, `ID_zvirete`, `ID_nemoci`, `upresneni_diagnozy`, `datum_zahajeni_lecby`, `stav`, `cena`) VALUES
(1, 14, 1, 2, NULL, '2014-11-02', 'dokončená', NULL),
(1, 22, 3, 1, NULL, '2015-07-01', 'dokončená', NULL),
(1, 23, 4, 3, NULL, '2015-05-21', 'započatá', NULL),
(1, 14, 5, 1, NULL, '2015-09-01', 'započatá', NULL),
(2, 14, 1, 3, NULL, '2015-04-27', 'započatá', NULL),
(2, 22, 4, 1, NULL, '2015-05-13', 'započatá', NULL),
(3, 23, 1, 1, NULL, '2015-08-02', 'započatá', NULL);

--
-- Triggers `lecba`
--
DELIMITER $$
CREATE TRIGGER `trig_poradove_cislo_lecby` BEFORE INSERT ON `lecba`
 FOR EACH ROW SET NEW.poradove_cislo_lecby = (SELECT COUNT(*) FROM lecba WHERE ID_zvirete = NEW.ID_zvirete) + 1
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `lecba_lek`
--

CREATE TABLE IF NOT EXISTS `lecba_lek` (
  `ID_zamestnance` int(10) unsigned DEFAULT NULL,
  `poradove_cislo_lecby` int(10) unsigned NOT NULL,
  `ID_zvirete` int(10) unsigned NOT NULL,
  `ID_leku` int(10) unsigned NOT NULL,
  `datum_nasazeni` date DEFAULT NULL,
  `doba_podavani` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `davkovani` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lecba_lek`
--

INSERT INTO `lecba_lek` (`ID_zamestnance`, `poradove_cislo_lecby`, `ID_zvirete`, `ID_leku`, `datum_nasazeni`, `doba_podavani`, `davkovani`) VALUES
(22, 1, 1, 3, '2015-12-07', NULL, NULL),
(21, 1, 1, 4, '2015-12-07', NULL, NULL),
(22, 1, 3, 1, '2015-12-07', NULL, NULL),
(NULL, 1, 4, 1, '2015-12-07', NULL, NULL),
(NULL, 1, 4, 2, '2015-12-07', NULL, NULL),
(NULL, 1, 5, 1, '2015-12-07', NULL, NULL),
(NULL, 1, 5, 5, '2015-12-07', NULL, NULL),
(NULL, 2, 1, 1, '2015-12-07', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lek`
--

CREATE TABLE IF NOT EXISTS `lek` (
  `ID_leku` int(10) unsigned NOT NULL,
  `nazev` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `typ` enum('mast','tablety','kapky','čípek','jiný') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ucinna_latka` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kontraindikace` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lek`
--

INSERT INTO `lek` (`ID_leku`, `nazev`, `typ`, `ucinna_latka`, `kontraindikace`) VALUES
(1, 'Endiaron', 'tablety', NULL, NULL),
(2, 'ALAVIS', 'mast', NULL, NULL),
(3, 'FAKTU', 'mast', 'policresulen, cinchocain', NULL),
(4, 'PROCTO-GLYVENOL', 'mast', 'tribenosid, lidokain', NULL),
(5, 'SPOFAX', 'čípek', 'cinchokain', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lek_nemoc`
--

CREATE TABLE IF NOT EXISTS `lek_nemoc` (
  `ID_leku` int(10) unsigned NOT NULL,
  `ID_nemoci` int(10) unsigned NOT NULL,
  `delka_lecby` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `procento_uspesne_lecby` decimal(5,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lek_nemoc`
--

INSERT INTO `lek_nemoc` (`ID_leku`, `ID_nemoci`, `delka_lecby`, `procento_uspesne_lecby`) VALUES
(1, 1, 'dva týdny', '95.50'),
(1, 3, 'moc dlouhá :-D', NULL),
(2, 1, NULL, NULL),
(2, 2, 'týden až dva', '15.48'),
(2, 3, NULL, NULL),
(3, 1, NULL, NULL),
(3, 3, 'měsíc až dva, někdy celý život', NULL),
(4, 2, NULL, NULL),
(5, 2, NULL, NULL),
(5, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `majitel`
--

CREATE TABLE IF NOT EXISTS `majitel` (
  `ID_majitele` int(10) unsigned NOT NULL,
  `jmeno` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prijmeni` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ulice` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mesto` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `psc` decimal(5,0) DEFAULT NULL,
  `cislo_uctu` decimal(14,0) DEFAULT NULL,
  `typ` enum('fyzická osoba','právnická osoba') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rodne_cislo` decimal(10,0) DEFAULT NULL,
  `cislo_OP` decimal(9,0) DEFAULT NULL,
  `ICO` decimal(8,0) DEFAULT NULL,
  `DIC` decimal(12,0) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `majitel`
--

INSERT INTO `majitel` (`ID_majitele`, `jmeno`, `prijmeni`, `ulice`, `mesto`, `psc`, `cislo_uctu`, `typ`, `rodne_cislo`, `cislo_OP`, `ICO`, `DIC`) VALUES
(1, 'Pavel', 'Juhaňák', 'Sportovní 407', 'Hanušovice', '78833', NULL, 'fyzická osoba', '9308020000', NULL, NULL, NULL),
(2, 'Stanislava', 'Pilařová', 'Fugnerova 816', 'Knežmost', '29402', NULL, 'fyzická osoba', NULL, NULL, NULL, NULL),
(3, 'Václav', 'Toušek', 'Rvačov 358', 'Roudnice nad Labem ', '41301', NULL, 'fyzická osoba', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `nemoc`
--

CREATE TABLE IF NOT EXISTS `nemoc` (
  `ID_nemoci` int(10) unsigned NOT NULL,
  `nazev` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `priznaky` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nemoc`
--

INSERT INTO `nemoc` (`ID_nemoci`, `nazev`, `priznaky`) VALUES
(1, 'průjmové onemocnění', 'řídká stolice, malátnost'),
(2, 'poranění kůže', NULL),
(3, 'hemeroidy', 'krvácení konečníku');

-- --------------------------------------------------------

--
-- Table structure for table `pozice`
--

CREATE TABLE IF NOT EXISTS `pozice` (
  `ID_pozice` int(10) unsigned NOT NULL,
  `nazev` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hodinova_mzda` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pozice`
--

INSERT INTO `pozice` (`ID_pozice`, `nazev`, `hodinova_mzda`) VALUES
(1, 'Doktor', 40000),
(2, 'Sestra', 20000),
(3, 'Vedouci', 50000);

-- --------------------------------------------------------

--
-- Table structure for table `zamestnanec`
--

CREATE TABLE IF NOT EXISTS `zamestnanec` (
  `ID_zamestnance` int(10) unsigned NOT NULL,
  `ID_pozice` int(10) unsigned NOT NULL,
  `jmeno` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prijmeni` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titul` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ulice` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mesto` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `psc` decimal(5,0) DEFAULT NULL,
  `cislo_uctu` decimal(14,0) DEFAULT NULL,
  `login` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `heslo` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `zamestnanec`
--

INSERT INTO `zamestnanec` (`ID_zamestnance`, `ID_pozice`, `jmeno`, `prijmeni`, `titul`, `ulice`, `mesto`, `psc`, `cislo_uctu`, `login`, `heslo`) VALUES
(14, 3, 'Jan', 'Herec', NULL, NULL, NULL, NULL, NULL, 'herech', '$2y$10$giXzCMHYUGZF6WgOfgvMsurin5QvpOdJCwkzsUXOfbmsGswwnFxw2'),
(21, 2, 'sestra', 'sestra', NULL, NULL, NULL, NULL, NULL, 'sestra', '$2y$10$m3c8MdB7W3sPIEgqRVo1K.a7k6Ae/mB/THeDYAQotHTpLFYUAqKkK'),
(22, 1, 'doktor', 'doktor', NULL, NULL, NULL, NULL, NULL, 'doktor', '$2y$10$5sv5IxUzGcNs8HL2mfyIDOlHbWuEN.rrq7UDoZshNsZLSHT18Fp9m'),
(23, 3, 'vedouci', 'vedouci', NULL, NULL, NULL, NULL, NULL, 'vedouci', '$2y$10$uExPO9unQuHxjXUHP5eAQ.IT7KzdRMj1C22VQJXvhk3BOMS6uJgt2');

-- --------------------------------------------------------

--
-- Table structure for table `zvire`
--

CREATE TABLE IF NOT EXISTS `zvire` (
  `ID_zvirete` int(10) unsigned NOT NULL,
  `ID_majitele` int(10) unsigned NOT NULL,
  `ID_druhu` int(10) unsigned NOT NULL,
  `jmeno` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `datum_narozeni` date DEFAULT NULL,
  `pohlavi` enum('samec','samice') COLLATE utf8mb4_unicode_ci NOT NULL,
  `vaha` int(10) unsigned DEFAULT NULL,
  `barva` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dat_posl_prohl` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `zvire`
--

INSERT INTO `zvire` (`ID_zvirete`, `ID_majitele`, `ID_druhu`, `jmeno`, `datum_narozeni`, `pohlavi`, `vaha`, `barva`, `dat_posl_prohl`) VALUES
(1, 1, 1, 'Amor', '2002-03-20', 'samec', 16, 'zlatá', '2010-10-21'),
(3, 1, 3, 'Žofka', '2013-08-16', 'samice', NULL, 'zeleno - žlutá', '2001-01-20'),
(4, 3, 1, 'Guláš', '2013-09-16', 'samec', NULL, 'černá', NULL),
(5, 2, 7, 'Pepa', '2015-12-07', 'samec', NULL, NULL, '2015-12-07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `druh`
--
ALTER TABLE `druh`
  ADD PRIMARY KEY (`ID_druhu`);

--
-- Indexes for table `druh_lek`
--
ALTER TABLE `druh_lek`
  ADD PRIMARY KEY (`ID_druhu`,`ID_leku`),
  ADD KEY `fk_druh_lek_lek` (`ID_leku`);

--
-- Indexes for table `lecba`
--
ALTER TABLE `lecba`
  ADD PRIMARY KEY (`poradove_cislo_lecby`,`ID_zvirete`),
  ADD KEY `fk_lecba_zamestnanec` (`ID_zamestnance`),
  ADD KEY `fk_lecba_zvire` (`ID_zvirete`),
  ADD KEY `fk_lecba_nemoc` (`ID_nemoci`);

--
-- Indexes for table `lecba_lek`
--
ALTER TABLE `lecba_lek`
  ADD PRIMARY KEY (`poradove_cislo_lecby`,`ID_zvirete`,`ID_leku`),
  ADD KEY `fk_lecba_lek_zamestnanec` (`ID_zamestnance`),
  ADD KEY `fk_lecba_lek_lek` (`ID_leku`);

--
-- Indexes for table `lek`
--
ALTER TABLE `lek`
  ADD PRIMARY KEY (`ID_leku`);

--
-- Indexes for table `lek_nemoc`
--
ALTER TABLE `lek_nemoc`
  ADD PRIMARY KEY (`ID_leku`,`ID_nemoci`),
  ADD KEY `fk_lek_nemoc_nemoc` (`ID_nemoci`);

--
-- Indexes for table `majitel`
--
ALTER TABLE `majitel`
  ADD PRIMARY KEY (`ID_majitele`);

--
-- Indexes for table `nemoc`
--
ALTER TABLE `nemoc`
  ADD PRIMARY KEY (`ID_nemoci`);

--
-- Indexes for table `pozice`
--
ALTER TABLE `pozice`
  ADD PRIMARY KEY (`ID_pozice`);

--
-- Indexes for table `zamestnanec`
--
ALTER TABLE `zamestnanec`
  ADD PRIMARY KEY (`ID_zamestnance`),
  ADD KEY `fk_zam_poz` (`ID_pozice`);

--
-- Indexes for table `zvire`
--
ALTER TABLE `zvire`
  ADD PRIMARY KEY (`ID_zvirete`),
  ADD KEY `fk_zvi_maj` (`ID_majitele`),
  ADD KEY `fk_zvi_druh` (`ID_druhu`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `druh`
--
ALTER TABLE `druh`
  MODIFY `ID_druhu` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `lek`
--
ALTER TABLE `lek`
  MODIFY `ID_leku` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `majitel`
--
ALTER TABLE `majitel`
  MODIFY `ID_majitele` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `nemoc`
--
ALTER TABLE `nemoc`
  MODIFY `ID_nemoci` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pozice`
--
ALTER TABLE `pozice`
  MODIFY `ID_pozice` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `zamestnanec`
--
ALTER TABLE `zamestnanec`
  MODIFY `ID_zamestnance` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `zvire`
--
ALTER TABLE `zvire`
  MODIFY `ID_zvirete` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `druh_lek`
--
ALTER TABLE `druh_lek`
  ADD CONSTRAINT `fk_druh_lek_druh` FOREIGN KEY (`ID_druhu`) REFERENCES `druh` (`ID_druhu`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_druh_lek_lek` FOREIGN KEY (`ID_leku`) REFERENCES `lek` (`ID_leku`) ON DELETE CASCADE;

--
-- Constraints for table `lecba`
--
ALTER TABLE `lecba`
  ADD CONSTRAINT `fk_lecba_nemoc` FOREIGN KEY (`ID_nemoci`) REFERENCES `nemoc` (`ID_nemoci`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_lecba_zamestnanec` FOREIGN KEY (`ID_zamestnance`) REFERENCES `zamestnanec` (`ID_zamestnance`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_lecba_zvire` FOREIGN KEY (`ID_zvirete`) REFERENCES `zvire` (`ID_zvirete`) ON DELETE CASCADE;

--
-- Constraints for table `lecba_lek`
--
ALTER TABLE `lecba_lek`
  ADD CONSTRAINT `fk_lecba_lek_lecba` FOREIGN KEY (`poradove_cislo_lecby`, `ID_zvirete`) REFERENCES `lecba` (`poradove_cislo_lecby`, `ID_zvirete`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_lecba_lek_lek` FOREIGN KEY (`ID_leku`) REFERENCES `lek` (`ID_leku`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_lecba_lek_zamestnanec` FOREIGN KEY (`ID_zamestnance`) REFERENCES `zamestnanec` (`ID_zamestnance`) ON DELETE CASCADE;

--
-- Constraints for table `lek_nemoc`
--
ALTER TABLE `lek_nemoc`
  ADD CONSTRAINT `fk_lek_nemoc_lek` FOREIGN KEY (`ID_leku`) REFERENCES `lek` (`ID_leku`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_lek_nemoc_nemoc` FOREIGN KEY (`ID_nemoci`) REFERENCES `nemoc` (`ID_nemoci`) ON DELETE CASCADE;

--
-- Constraints for table `zamestnanec`
--
ALTER TABLE `zamestnanec`
  ADD CONSTRAINT `fk_zam_poz` FOREIGN KEY (`ID_pozice`) REFERENCES `pozice` (`ID_pozice`) ON DELETE CASCADE;

--
-- Constraints for table `zvire`
--
ALTER TABLE `zvire`
  ADD CONSTRAINT `fk_zvi_druh` FOREIGN KEY (`ID_druhu`) REFERENCES `druh` (`ID_druhu`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_zvi_maj` FOREIGN KEY (`ID_majitele`) REFERENCES `majitel` (`ID_majitele`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
